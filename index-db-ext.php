<?php
/*
Plugin Name: Wordpress Database Extension Tables
Version: 0.1
Plugin URI: http://ken.net/wordpress
Description: Provide a high performance mechanism to add structured data to post data.
Author: Ken Snyder
Author URI: http://ken.net
*/
/**
 * Copyright (c) 2012 Ken Snyder. All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * **********************************************************************
 */
global $wpdb;
define( 'WP_DATABASE_EXTENSION', '0.6' );
define( 'WP_DBX_DIR' , dirname (__FILE__) );
define( 'DBX_PREFIX', 'ext_table_'); // the prefix that all extension tables will have
define( 'WP_DBX_PREFIX' , $wpdb->prefix . DBX_PREFIX ); // add the WP prefix to the plug-in prefix for a fully qualified prefix
define( 'DBX_LOCALISATION' , 'dbx_localisation' );
// error definitions
define ( 'DBX_ERROR_CREATE_TABLE_EXISTS' , 1001 );
define ( 'DBX_ERROR_CREATE_TABLE_NOT_DEFINED' , 1002 );
define ( 'DBX_ERROR_CREATE_TABLE_UNKNOWN_ERROR' , 1003 );

define ( 'DBX_ERROR_CREATE_TABLE_SQL_EXEC_ERROR' , 1005 );
define ( 'DBX_ERROR_ADD_COL_SQL_EXEC_ERROR' , 1006 );

define ( 'DBX_ERROR_ADD_COL_NO_TABLE' , 1010 );
define ( 'DBX_ERROR_ADD_COL_ALREADY_EXISTS' , 1011 );
define ( 'DBX_ERROR_ADD_COL_UNKNOWN_ERROR' , 1012 );

// include() or require()
require_once ( WP_DBX_DIR . '/includes/class.DBX_BaseExtension.php');
require_once ( WP_DBX_DIR . '/includes/class.DBX_UtilityFunctions.php');
require_once ( WP_DBX_DIR . '/includes/class.DBX_DataAccess.php');
require_once ( WP_DBX_DIR . '/includes/class.DBX_AdminPage.php');
require_once ( WP_DBX_DIR . '/includes/class.DBX_PostMetaBox.php');
require_once ( WP_DBX_DIR . '/includes/class.DBX_TableExtension.php');
require_once ( WP_DBX_DIR . '/includes/class.DBX_FieldGrouping.php');
require_once ( WP_DBX_DIR . '/includes/class.DBX_AbstractionMap.php');
require_once ( WP_DBX_DIR . '/includes/class.DBX_Configurator.php');
require_once ( WP_DBX_DIR . '/includes/class.DBX_ConfigGrouping.php');
require_once ( WP_DBX_DIR . '/includes/class.DBX_Meta.php');
require_once ( WP_DBX_DIR . '/includes/class.DBX_Resultset.php');

// Enqueue Scripts and Styles
add_action( 'admin_enqueue_scripts' , 'add_dependant_scripts_and_styles' , 10 );

// Wordpress hooks and loadtime functions
add_action ( 'admin_menu' , "DBX_AdminPage::init", 10, 2);
add_action ( 'add_meta_boxes' , "DBX_PostMetaBox::register_meta_box" );
add_action ( 'admin_head' , 'DBX_PostMetaBox::register_custom_scripts' );
add_action ( 'admin_menu' , 'DBX_PostMetaBox::remove_taxonomy_boxes' );
add_action ( 'save_post' , 'DBX_PostMetaBox::save_custom_meta', 10, 1 );

//add_action ( 'template_redirect' , 'initialise');
add_action ( 'init' , 'initialise');

function initialise () {
	// instantiate global extension object
	global $wpdb_x;
	$wpdb_x = new DBX_TableExtension ();
	// Call init action so that implementations of this plugin can configure themselves
	do_action ( 'wp_dbx_init' );
	do_action ( 'wp_dbx_post_type_setup'); // allows setup of custom post types (and taxonomies) before post-type to ext-table mapping is done
	do_action ( 'wp_dbx_mapping' );
	do_action ( 'wp_dbx_metaboxes' );
}

function add_dependant_scripts_and_styles () {
	wp_enqueue_script('dbx', plugins_url( 'js/DBX.js' , __FILE__ ) );
	wp_enqueue_style ('dbx-css', plugins_url ('css/dbx.css' , __FILE__ ) );
				
	// it's all dependant on jQuery so let's include that in the stack
	// wp_enqueue_script ( 'jquery' );
	// Bootstrap
	// ------------			
	// wp_enqueue_script ( 'bootstrap-tab', plugins_url ( 'dependencies/bootstrap-tab.js' , __FILE__) , 'jquery' , false , true );
	// wp_enqueue_script ( 'bootstrap-alert', plugins_url ( 'dependencies/bootstrap-alert.js' , __FILE__) , 'jquery' , false , true );
	// wp_enqueue_script ( 'bootstrap-tooltip', plugins_url ( 'dependencies/bootstrap-tooltip.js' , __FILE__) , 'jquery' , false , true );
	// wp_enqueue_script ( 'bootstrap-button', plugins_url ( 'dependencies/bootstrap-button.js' , __FILE__) , 'jquery' , false , true );
	// wp_enqueue_script ( 'bootstrap-transition', plugins_url ( 'dependencies/bootstrap-transition.js' , __FILE__) , 'jquery' , false , true );

	// wp_enqueue_script ( 'jquery-ui', plugins_url ( 'dependencies/jquery-ui-1.8.21.custom.min.js' , __FILE__) , 'jquery' , false , true );
	// wp_enqueue_script ( 'jquery-ui-timepicker', plugins_url ( 'dependencies/jquery-ui-timepicker-addon.js' , __FILE__) , 'jquery' , false , true );
	// wp_enqueue_script ( 'jquery-ui-sliderAccess', plugins_url ( 'dependencies/jquery-ui-sliderAccess.js' , __FILE__) , 'jquery' , false , true );
	// STYLES
	// wp_enqueue_style ('bootstrap-css', plugins_url ('dependencies/bootstrap-admin.css' , __FILE__ ) );

	// wp_enqueue_style ('bootstrap-timepicker-css', plugins_url ('dependencies/timepicker.css' , __FILE__ ) );
	// wp_enqueue_style ('bootstrap-ui-timepicker-css', plugins_url ('dependencies/jquery-ui-1.8.21.custom.css' , __FILE__ ) );
	// wp_enqueue_style ('jquery-ui-timepicker-css', plugins_url ('dependencies/jquery-ui-timepicker-addon.css' , __FILE__ ) );
	
	// PostMeta dependencies
	// wp_enqueue_script('jquery-ui-datepicker');
	// wp_enqueue_script('jquery-ui-slider');
	// wp_enqueue_style ('jquery-ui-custom', plugins_url ('css/jquery-ui-custom.css' , __FILE__  ) );
	
	
}

/* END OF FILE */