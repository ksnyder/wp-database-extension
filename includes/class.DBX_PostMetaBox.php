<?php
	
/**
* DBX_PostMetaBox class
*
* A set of static library functions designed to build the meta_box that post-types will use in the admin panels to 
* show the extention table data
*
* @package database-extension-tables
* @author Ken Snyder
**/
class DBX_PostMetaBox 
{
	// Const
	
	// Instance Variables

	// Static Functions
	public static function register_meta_box() {
		global $wpdb_x;
		if (class_exists ('AppLogger') ) AppLogger::log_call_stack ();
		$post_types = $wpdb_x->get_post_types_extended();
		
		foreach ($post_types as $cpt) {
			add_meta_box (
			'dbx_meta_box', // $id
			'Extension Table Data', // $title 
			'DBX_PostMetaBox::show_meta_box', // $callback
			$cpt, // $page
			'normal', // $context
			'high'); // $priority
		}
	}
		
	public static function register_custom_scripts() {
		// removed this, wasn't necessary
	}
			
	
	// The Callback
	public static function show_meta_box() {
		global $post, $wpdb_x;
		
		$page = FE_Template::init();
		$page->assign ( 'wpdb_x' , $wpdb_x);
		// Get meta data for the post in question
		$post_id = $wpdb_x->get_base_id ( $post->ID );
		$meta = $wpdb_x->get_post_meta( $post_id );
		$page->assign ('post_id' , $post_id );
		$page->assign ('nonce' , wp_create_nonce(basename(__FILE__)) );
		$page->assign ( 'meta', $meta );
		// Display the template
		$page->display( "admin_pages/post_meta_box.tmpl");
	}
	
	public static function remove_taxonomy_boxes() {
		remove_meta_box('categorydiv', 'post', 'side');
	}
	

	// Save the Data
	public static function save_custom_meta( $post_id ) {
		global $wpdb_x;
		// check whether this post type is registered for extension tables. If it is not then exit.
		if ( !in_array ( get_post_type ( $post_id ) , $wpdb_x->get_post_types_extended() ) ) return false;
		// TODO: This is causing problems; have commented out for now but need to understand better and bring back in if required!
		if ( !isset ( $_POST['custom_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['custom_meta_box_nonce'] , basename(__FILE__)) ) 
			return $post_id;
		
		// check autosave
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return $post_id;
		// check permissions
		if ('page' == $_POST['post_type']) {
			if (!current_user_can('edit_page', $post_id))
			return $post_id;
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id;
		}
		
		// now let's focus on the root ID because that's what the extension tables attach to
		// TODO: need to evaluate this design decision
		$post_id = $wpdb_x->get_base_id ( $post_id );

		// iterate through the field values and add the ones that have 
		// changed to two arrays: names + values
		$values = array ();
		$columns = array ();
		// Get keys but exclude hidden fields and attributes (TODO: should I bring attributes back in?)
		$custom_meta_fields = $wpdb_x->get_post_type_custom_keys ( $post_id , get_post_type ( $post_id ) , false , false );
		$changed_fields = ___::compact( preg_split ('/,/' , $_POST['changed_fields']) );
		$nullified_fields = ___::compact( preg_split ('/,/' , $_POST['nullified_fields']) );
		if (class_exists ('AppLogger') ) AppLogger::debug ( "\nChanged Fields\n" . print_r($changed_fields, TRUE) ); 
		if (class_exists ('AppLogger') ) AppLogger::debug ( "\nNullified Fields\n" . print_r($nullified_fields, TRUE) ); 
		
		$changed_dictionary = array();
		foreach ($changed_fields as $changed_field) {
			$changed_dictionary[$changed_field] = $_POST[$changed_field];
		}
		if (class_exists ('AppLogger') ) AppLogger::debug ( "CALLING update_extension_tables" . print_r ( $changed_dictionary , TRUE ) ); 
		$wpdb_x->update_extension_tables ( $post_id, $changed_dictionary , $nullified_fields);											
	}

} // END class 
	
/* ---------------- END OF FILE ------------------- */