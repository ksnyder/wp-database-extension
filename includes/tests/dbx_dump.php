<?php
include "../../../lg-frontend-classes/includes/AddWordpressProxy.php";
define ('LG_DUMP_FILES' , dirname(__FILE__) );

/**
 * This class was used to pull out the current configuration information and start to map it to a more JSON based configuration model
 *
 * @package  		database-extension-tables
 * @copyright		LifeGadget Limited, 2013. All rights reserved.
*/
class DBX_Dump extends DBX_BaseExtension {

	public function __construct() {
		parent::__construct();
		
		// TABLE STRUCTURE
		$fh = fopen( LG_DUMP_FILES . "/table_structure.json", 'w')
			or die("Error opening table_structure file");
		fwrite($fh, json_encode($this->ptr_extension_tables));
		fclose($fh);
		// GROUPING OBJECTS
		$fh = fopen( LG_DUMP_FILES . "/group_objects.json", 'w')
			or die("Error opening group_objects file");
		fwrite($fh, json_encode($this->ptr_grouping_reference));
		fclose($fh);
		// MAPPING TABLES
		$fh = fopen( LG_DUMP_FILES . "/mapping_tables.json", 'w')
			or die("Error opening mapping_tables file");
		fwrite($fh, json_encode($this->ptr_mapping_tables));
		fclose($fh);
		
		
		// NOW Build a NEW STRUCTURE
		
		
	}
}

$foobar = new DBX_Dump();