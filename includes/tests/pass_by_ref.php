<?php

	class TestClass {

		private $my_precious = array ('one','two','three');
	
		public function &give_reference() {
			return $this->my_precious;
		}

	}

	$foobar = new TestClass();
	$my_ref =& $foobar->give_reference();
	echo "My reference is: " . json_encode($my_ref) . "\n\n";
	$my_ref = array ("four", "five", "six");

	echo print_r($foobar,true);