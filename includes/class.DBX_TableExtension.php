<?php
/**
* @package database-extension-tables
* @author Ken Snyder
**/

include_once ( WP_DBX_DIR . '/includes/class.DBX_DataAccess.php');
define ( 'WP_DBX_CACHE_GROUP' , 'dbx_database_extension');

/**
 * DB_Ext_TableExtension class
 *
 * Responsible for:
 * 		a) storing all of the configuration/state information about extension tables and the mapping between post-types and extension tables
 *		b) all getting and setting interactions with the database
 *
 * It exposes itself through a set of public static methods but also exposes a protected interface so that the PostTypeExtention can act
 * as a controller of it (which in turn exposes a more user friendly api than the TableExtension would provide by itself)
 *
 * @package database-extension-tables
 * @author Ken Snyder
 **/
class DBX_TableExtension
{
	// Const

	// Instance Variables
	private $extension_tables;
	private $mapping_tables;
	private $grouping_objects;
	private $abstraction_maps;
	
	// Constructor
	function __construct () {
		$this->extension_tables = array();
		$this->grouping_objects = array();
	}
	
	// Passing references to state
	public function &get_table_reference() {
		return $this->extension_tables;
	}
	
	public function &get_grouping_reference() {
		return $this->grouping_objects;
	}

	public function &get_mapping_tables() {
		return $this->mapping_table;
	}
	

	public function load_configuration ($json_config) {
		$this->extension_tables = json_decode (file_get_contents($json_config), true);
	}
		
	/**
	 * Save Configuration
	 * 
	 * Saves the DBX configuration back to the JSON config file. If no ext_table is given then it assumed you are providing the entire configuration object. 
	 * If you do provide an ext_table than you must provide that DBX tables full set of column meta. In most cases, the admin screen will specify both *ext_table*
	 * and  
	 *
	 * @param		json		$json		the json-encoded configuration
	 * @param		string		$ext_table 	(opt) the DBX table you are providing configuration information for
	 * @param		string		$column 	(opt) the column in a given DBX table that you're providing meta information for
	 * @return		void
	*/
	public function save_configuration() {
		// open a file handle
		$fh = fopen(LG_DBX_TABLE_CONFIGURATION, 'w')
			or die("Error opening dbx configuration file");
		fwrite($fh, json_encode($this->extension_tables));
		fclose($fh);
	}
	
	public function load_groupings ($json_config) {
		$groupings = json_decode (file_get_contents($json_config), true);
		foreach($groupings as $group) {
			foreach ($group as $field => $value) {
				$this->grouping_objects[$value['ext_table']][$field] = new DBX_FieldGrouping($value['ext_table'],$value['slug_name'],$value['group_name']);
				if (count($value['group_attributes']['fields'])>0) 
					$this->grouping_objects[$value['ext_table']][$field]->add_fields($value['group_attributes']['fields']);
				if (count($value['group_attributes']['filters'])>0) {
					foreach ($value['group_attributes']['filters'] as $filtername=>$list) {
						$this->grouping_objects[$value['ext_table']][$field]->add_filter($filtername,$list);
					}
				}
			}
		}
	}

	
	// Configuration Methods
	public function add_extension_table ( $ext_table , $options_hash = array() , $include_fk = true ) {
		$default_table_attributes = array ( 'auto-hook-use' => 'false' , 'dynamic-attributes' => 'false' );
		$options_hash = array_merge ( $default_table_attributes , $options_hash );
		if ( !isset( $this->extension_tables [ $ext_table ] ) ) {
			$this->extension_tables [ $ext_table ] = $options_hash;
			$this->add_extension_cols ( $ext_table , array (
					'id' => array ( 'type' => "bigint(20)" , 'display-type' => 'pk', 'desc' => "primary key for " . $ext_table )
			));
			$this->add_pk ( $ext_table , 'id' );
			if ( $include_fk ) {
				$this->add_extension_cols ( $ext_table , array(
					'post_id' => array ( 'type' => "bigint(20)" , 'display-type' => 'fk(wp_posts.id)' , 'desc' => "foreign key to wp_posts (aka, the base transaction) " )
				));
				$this->add_fk ( $ext_table , 'post_id' , 'wp_posts.id' );
				$this->hide_in_admin ( $ext_table , 'post_id');		
			}
			$this->add_extension_cols($ext_table,array("json_ext_{$ext_table}" => array ( 'type' => "longtext" , 'display-type' => 'json' , 'desc' => "extensibility hash for " . $ext_table )));
			$this->extension_tables[$ext_table]['type'] = 'transactional';	// by default all extension tables are "transactional"
		} else {
			error_log ( 'Tried to add an extension table (' . $ext_table . ') which already is defined. Exiting without re-adding.' );
		}
		return new DBX_Configurator ($ext_table, $this->extension_tables);
	}
	
	/** Allows the table to marked as a reference table. This is just a meta property of the table that helps manage it's use in certain cases **/
	public function mark_as_reference_table ( $ext_tables ) {
		$ext_tables = LG_Utility::make_into_array($ext_tables);
		foreach ($ext_tables as $ext_table) {
			$this->extension_tables[$ext_table]['type'] = 'reference';
		}
	}
	
	/**
	* add_pk
	* 
	* allows the developer to specify which column is THE primary key
	*
	* @param string $ext_table the name of the extension table which you are specifying the PK for
	* @param string $col_list in many cases there will only be one column but by having an array we allow for it to be multiple
	* @return none
	* @author Ken Snyder
	*/
	public function add_pk ( $ext_table , $col_name ) {
		if ( !isset ( $this->extension_tables [ $ext_table ][ 'columns'][ $col_name ] ) ) {
			error_log ( "Trying to set the primary key of $ext_table to $col_name but $col_name is not a valid column!");
			return false;
		} else {
			$this->extension_tables [ $ext_table ]['columns'][ $col_name ]['PK'] = 'true';
			$this->extension_tables [ $ext_table ]['constraints']['PK'] = $col_name;
			$this->add_not_null_constraint ( $ext_table , array($col_name) );
			$this->add_unsigned_constraint ( $ext_table , array($col_name) );
			$this->hide_in_admin ( $ext_table , $col_name );
		}
	}
	
	/**
	* add_fk
	* 
	* allows the developer to specify that a column is a foreign key
	*
	* @param string $ext_table the name of the extension table which has the FK reference in it
	* @param array $col_list in many cases there will only be one column but by having an array we allow for it to be multiple
	* @param string $target_key the table and column name of the reference destination. uses the format: "table.column"
	* @return none 
	* @author Ken Snyder
	*/
	public function add_fk ($ext_table , $col_name , $target_key ) {
		if ( !isset ( $this->extension_tables [ $ext_table ]['columns'][ $col_name ] ) ) {
			error_log ( "Trying to set a foreign key on $ext_table.$col_name but $col_name does not exist" );
			return false;
		} else {
			// adding this to both the "contraints" array and the "columns" array for easier accessibility
			$this->extension_tables [ $ext_table ][ 'constraints'][ 'FK' ][ $col_name ] = $target_key;
			$this->extension_tables [ $ext_table ][ 'columns' ][ $col_name ][ 'FK' ] = $target_key;
			// FK's should also be unsigned if they're type is set to an integer value
			if ( DBX_DataAccess::get_family_type ($this->extension_tables [ $ext_table ][ 'columns' ][ $col_name ][ 'type' ] ) == "integer" ) {
				$this->add_unsigned_constraint ( $ext_table , array($col_name) );
			}
			return true;
		}
	}
	
	/**
	* is_fk
	* 
	* tests whether a given table/column is a foreign key or not
	*
	* @param string $ext_table the name of the extension table 
	* @param string $col_name the name of the column being questioned
	* @return boolean 
	* @author Ken Snyder
	*/
	public function is_fk ($ext_table, $col_name ) {
		$fk_list = $this->extension_tables[ $ext_table ]['constraints']['FK'];
		foreach ($fk_list as $fk => $value) {
			if ($fk == $col_name) return true;
		}
		return false;
	}

	/**
	* is_pk
	* 
	* tests whether a given table/column is a primary key or not
	*
	* @param string $ext_table the name of the extension table 
	* @param string $col_name the name of the column being questioned
	* @return boolean 
	* @author Ken Snyder
	*/
	public function is_pk ($ext_table, $col_name ) {
		if ( $this->extension_tables[ $ext_table ]['constraints']['PK'] == $col_name ) return true;
		else return false;
		
	}

	
	/**
	* add_not_null_constraint
	* 
	* adds a NOT NULL constraint to a set of columns
	*
	* @param string $ext_table The name of the extension table where the rows to be constrained are located
	* @param array $col_list The list of columns who should be set to "NOT NULL"
	* @return boolean returns true if successful, otherwise false
	* @author Ken Snyder
	*/
	public function add_not_null_constraint ($ext_table , array $col_list ) {
		if ( !isset ( $this->extension_tables [$ext_table] ) ) {
			error_log ( 'Trying to add a NOT NULL contraint to extension table ' . $ext_table . ' but table is not defined!' );
			return false;
		} else {
			// iterate through columns
			$error_free = true;
			foreach ($col_list as $col) {
				if ( !isset ( $this->extension_tables [$ext_table]['columns'][$col] ) ) {
					error_log ('Trying to add a NOT NULL constraint to ' . $ext_table . '.' . $col . ' but this column is not defined!' );
					$error_free = false;
				} else {
					// set this into both the "columns" and "constraints" section; this is redundant but may
					// be useful structurally in some future case. If not then this can be removed from "constraints"
					$this->extension_tables [ $ext_table ][ 'columns' ][ $col ][ 'nullable' ] = 'NOT NULL';
					$this->extension_tables [ $ext_table ][ 'constraints' ][ 'NOT_NULL' ][ $col ] = 'true';
				}
			}
			return $error_free;
		}
	}
	
	/**
	* add_unsigned_constraint
	* 
	* adds a "unsigned" constraint to a set of columns
	*
	* @param string $ext_table The name of the extension table where the rows to be constrained are located
	* @param array $col_list The list of columns who should be set to "NOT NULL"
	* @return boolean returns true if successful, otherwise false
	* @author Ken Snyder
	*/
	public function add_unsigned_constraint ($ext_table , array $col_list ) {
		if ( !isset ( $this->extension_tables [$ext_table] ) ) {
			error_log ( 'Trying to add an  UNSIGNED contraint to extension table ' . $ext_table . ' but table is not defined!' );
			return false;
		} else {
			// iterate through columns
			$error_free = true;
			foreach ($col_list as $col) {
				if ( !isset ( $this->extension_tables [$ext_table]['columns'][$col] ) ) {
					error_log ('Trying to add a "unsigned" constraint to ' . $ext_table . '.' . $col . ' but this column is not defined!' );
					$error_free = false;
				} else {
					// set this into both the "columns" and "constraints" section; this is redundant but may
					// be useful structurally in some future case. If not then this can be removed from "constraints"
					$this->extension_tables [ $ext_table ][ 'columns' ][ $col ][ 'unsigned' ] = 'unsigned';
					$this->extension_tables [ $ext_table ][ 'constraints' ][ 'unsigned' ][ $col ] = 'true';
				}
			}
			return $error_free;
		}
	}
	/** removes an unsigned constraint from a column **/
	public function remove_unsigned_constraint ($ext_table , array $col_list ) {
		if ( !isset ( $this->extension_tables [$ext_table] ) ) {
			error_log ( 'Trying to remove a NOT NULL contraint to extension table ' . $ext_table . ' but table is not defined!' );
			return false;
		} else {
			// iterate through columns
			$error_free = true;
			foreach ($col_list as $col) {
				if ( !isset ( $this->extension_tables [$ext_table]['columns'][$col] ) ) {
					error_log ('Trying to remove a "unsigned" constraint to ' . $ext_table . '.' . $col . ' but this column is not defined!' );
					$error_free = false;
				} else {
					// set this into both the "columns" and "constraints" section; this is redundant but may
					// be useful structurally in some future case. If not then this can be removed from "constraints"
					unset ( $this->extension_tables [ $ext_table ][ 'columns' ][ $col ][ 'unsigned' ] );
					$this->extension_tables [ $ext_table ][ 'constraints' ][ 'unsigned' ][ $col ] = 'false';
				}
			}
			return $error_free;
		}
	}
	
	/**
	 * add_extension_cols
	 *
	 * This was the first and primary method for adding columns to the extension tables. 
	 *
	 * @author Ken Snyder
	 */
	public function add_extension_cols ( $ext_table , $column_hash ) { 
		if ( isset( $this->extension_tables [ $ext_table ]) ) {
			foreach ($column_hash as $col_id => $col_values) {
				// make $column a pointer to the appropriate part of the extension_tables data structure
				$column = &$this->extension_tables [ $ext_table ]['columns'][ $col_id ];
				$column = $col_values;
				$column ['id'] = $col_id; // this is redundant with the key name but makes some things easier (or at least clearer)
				$column ['key-type'] = 'db-column';
				$column ['ext-table'] = $ext_table;
				$column ['display-type'] = $this->get_default_display_type ( $column );
				if ( !isset ( $col_values['name'] ) ) $column['name'] = $col_id;
				// Make all integers -- by default -- unsigned
				if ( preg_match ( '/.*int.*/' , $column['type']) > 0 ) $this->add_unsigned_constraint ( $ext_table , array ( $col_id ) );
			}
		} else {
			error_log ( 'Trying to add columns to a table (' . $ext_table . ') which has not been set' );
		}
	}
	 
	
	// while defaults are set when added in add_extension_cols; certain things like enums aren't in place
	// so it helps to add this as a seperate call after all DDL is done
	public function add_display_defaults ( $ext_tables = null ) {
		if ( !isset ( $ext_tables ) ) $ext_tables = $this->get_extension_tables();
		else if ( !is_array ( $ext_tables ) ) $ext_tables = array ( $ext_tables );
		
		foreach ( $ext_tables as $ext_table ) {
			if ( isset( $this->extension_tables [ $ext_table ]) ) {
				$column_hash = $this->get_columns_meta( $ext_table );
				foreach ($column_hash as $col_name => $col_values) {
					$col_name ['display-type'] = $this->get_default_display_type ( $this->extension_tables [ $ext_table ]['columns'][ $col_name ] );
				}
			} else error_log ( "Warning: $ext_table wasn't found when trying to add display defaults." );
		}	
	}
	
	public function add_col_enum ( $ext_table , $col_name , $enum_array , $user_can_extend = 'false') { 
		//AppLogger::log_call_stack("col_enum"); 
		if ( !isset ( $this->extension_tables [ $ext_table ]['columns'][ $col_name ] )){
			error_log ( 'Trying to add an enumeration to an undefined column (' . $ext_table . '.' . $col_name . ')' );
		} else {
			// set display type to a 'select' list
			$this->set_display_type ( $ext_table , $col_name , 'select' );
			// convenience reference
			$column = &$this->extension_tables [ $ext_table ]['columns'][ $col_name ];
			$column ['enum'] = array (
				'user_extendable' => $user_can_extend,
				'attributes' => $enum_array
			);
			// check whether structure is [#] => [value] or [name] => [value]
			// TODO: This isn't working!
			foreach ( $column['enum']['attributes'] as $key => $value) {
				if ( is_int ($key) ) {
					// array_flip ( $key );
					$column['enum']['attributes'][$value] = $value;
					unset ( $column['enum']['attributes'][$key] );
				}
							
			}
		}
	}
	
	public function set_display_type ( $ext_table , $col_name , $display_type ) {
		if (!isset ( $this->extension_tables [ $ext_table ]['columns'][ $col_name ] ) ) {
			error_log ( 'Trying to set the display type of an undefined column (' . $ext_table . '.' . $col_name . ')' );
		} else {
			$this->extension_tables [ $ext_table ]['columns'][ $col_name ]['display-type'] = $display_type;
		}
	}
	
	/**
	* get_default_display_type
	* 
	* Helper function that "defaults" the display type if it's not been already set explicitly
	*
	* @param  hash 		$column The associative array for a given column
	* @return string 	The name of the display-type for this column
	* @author Ken Snyder
	*/
	public function get_default_display_type ( $column ) {
		// If not then switch on the database "type"		
		// list ( $db_type , $size ) =  explode ( "(" , $column['type'] , 2 );
		if ( isset ($column['display-type']) ) return $column['display-type'];
		
		if ( preg_match ( '/(.*)\((.*)\)/' , $column['type'] , $matches , PREG_OFFSET_CAPTURE ) ) {
			//error_log ( "MATCHES:" . print_r ( $matches , TRUE ) );
			$db_type =  strtolower ( $matches[1][0] );
			$size = $matches[2][0];
		}
		else $db_type = strtolower ( $column['type'] );
		$is_enumerated = isset ( $column ['enum'] );
		switch ( $db_type ) {
			case 'int':
			case 'integer':
			case 'bigint':
				return "text";
				break;
			case 'varchar':
				if ( $is_enumerated )
					return "select";
				else {
					if ( $size < 101 ) return "text";
					else return "textarea";
				}
				break;
			case 'text':
				return "text";
				break;
			case 'longtext':
				return "textarea";
				break;
			case 'datetime':
				return "date";
				break;
			default:
				return "text";
		}
	}
	
	/**
	* hide_in_admin
	* 
	* Some columns -- like id, post_id, and json_ext -- should be hidden in the admin panel's metabox.
	* This function lets you assign attributes which should be excluded.  
	*
	* @param string $ext_table The extension table whose attributes should be hidden
	* @return void 
	* @author Ken Snyder
	*/
	public function hide_in_admin ($ext_table , $hide_attributes ) {
		if ( !$this->ext_table_exists ($ext_table) ) {
			if (class_exists ('AppLogger') ) AppLogger::error ( "$ext_table extension table doesn't exist so can't hide attributes" ); 
			return false;
		}
		if ( !is_array ( $hide_attributes ) ) 
			$hide_attributes = array ( $hide_attributes );
		
		foreach ($hide_attributes as $attr) {
			$this->set_col_meta ( $ext_table , $attr , "admin_hide", "HIDE" );
		}
	}
	
	/**
	* is_hidden_field
	* 
	* Checks to see if column is "hidden" for the admin view
	*
	* @param string $ext_table The Extension Table
	* @return boolean True/false based on whether hidden or not
	* @author Ken Snyder
	*/
	public function is_hidden_field ($ext_table , $col) {
		$attr = $this->get_columns_meta ( $ext_table , $col );
		if ( isset ( $attr['admin_hide'] ) ) return true;
		else return false;
	}
	
	
	/**
	* ext_table_exists
	* 
	* Returns true/false as to whether the specified table exists as an extension table
	*
	* @param 	string 	 $table_name	Table name	
	* @return 	boolean	 				True/false on whether table exists
	* @author Ken Snyder
	*/
	public function ext_table_exists ( $table_name ) {
		if ( isset ($this->extension_tables[$table_name] ) ) return true;
		else return false;
	}
	
	/**
	* column_exists
	* 
	* returns true/false as to whether a column exists in a given extension table 
	*
	* @param string		$ext_table		The extension table to look in 
	* @param string		$column			The column to validate
	* @return boolean					True/false result
	**/
	public function column_exists ( $ext_table , $column ) {
		if ( in_array($column, $this->get_columns($ext_table)) ) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	* set_col_meta
	* 
	* Sets a meta attribute for a column in the extension table 
	*
	* @param 	string	$ext_table	The name of the extension table
	* @param	string	$col_name	The name of the column
	* @param	string	$name		The name of the meta attribute
	* @param	string	$value		The value of the meta attribute
	* @return Return_Type Return Description
	* @author Ken Snyder
	*/
	public function set_col_meta ( $ext_table , $col_name , $name , $value) {
		if ( !$this->ext_table_exists ($ext_table) ) {
			if (class_exists ('AppLogger') ) AppLogger::error ( "$ext_table extension table doesn't exist so can't set col meta" ); 
			return false;
		}
		if (!$this->column_exists($ext_table,$col_name)) {
			AppLogger::warning ( "$col_name column doesn't exist in {$ext_table} extension table, can't add meta attribute to it. " );
			return false;
		} else {
			$this->extension_tables[$ext_table]['columns'][$col_name][ $name ] = $value;
		}
	}
	
	/**
	* dump_log
	* 
	* dumps the 'extension_tables' instance variable to the error log
	*
	* @param void  
	* @return void 
	* @author Ken Snyder
	*/
	public function dump_log () {
		error_log ( "EXTENSION TABLE META:\n" . print_r ( $this->extension_tables , TRUE ) );
		error_log ( "MAPPING INFORMATION:\n" . print_r ( $this->mapping_tables , TRUE ) );
	}
	
	
	public function add_extension_attributes ( $ext_table , $attr_hash ) { 
		if ( isset ( $this->extension_tables [ $ext_table ][ 'attributes' ][ 'dictionary' ]  )) {
			error_log ( 'Notice: setting the attributes dictionary for ' . $ext_table . ' extension table although this has already been defined. Old values were:' . print_r ( $this->extension_tables [ $ext_table ][ 'attributes' ][ 'dictionary' ] , TRUE ) );
		}
		foreach ($attr_hash as $attr_name => $attr_values) {
			$this->extension_tables [ $ext_table ][ 'attributes' ][ 'dictionary' ][$attr_name] = $attr_values;
			$this->extension_tables [ $ext_table ][ 'attributes' ][ 'dictionary' ][$attr_name]['key-type'] = 'json-attribute';
			$this->extension_tables [ $ext_table ][ 'attributes' ][ 'dictionary' ][$attr_name]['ext-table'] = $ext_table;
		}
	}	
	
	public function attributes_extendible ( $ext_table, $user_can_extend = true , $user_can_add_to_dictionary = false ) {
		$this->extension_tables [ $ext_table ]['attributes']['extendible'] = $user_can_extend;
		$this->extension_tables [ $ext_table ]['attributes']['can_add_to_dictionary'] = $user_can_add_to_dictionary;
	}
	
	public function create_table ( $ext_table , $create_fk_constraint = false ) { 
		global $wpdb_x;
		if ( DBX_DataAccess::db_table_exists ( $ext_table ) ) {
			if (class_exists ('AppLogger') ) AppLogger::warning ( 'DDL: tried to create extension table "' . $ext_table . '" which already exists in the database' );
			throw new ErrorException ( "Extension table '{$ext_table}' already exists, could not add." , DBX_ERROR_CREATE_TABLE_EXISTS );
		} else if ( !$wpdb_x->ext_table_exists ( $ext_table ) ){
			if (class_exists ('AppLogger') ) AppLogger::error ( "DDL: trying to add an extension table -- '$ext_table' -- that has no definition." ); 
			throw new ErrorException ( "Extension table '{$ext_table}' is not defined in code so it can not be added to the database!" , DBX_ERROR_CREATE_TABLE_NOT_DEFINED );
		} else {
			if (class_exists ('AppLogger') ) AppLogger::log ( 'DDL: creating extension table: ' . $ext_table ); 
			$result = DBX_DataAccess::create_table ( $ext_table , $create_fk_constraint , $this->extension_tables [$ext_table] );
			if ( $result ) return $result;
			else throw new ErrorException ( "Creating new extension table failed in call to DBX_DataAccess::create_table()." , DBX_ERROR_CREATE_TABLE_UNKNOWN_ERROR );
		}
	} 
	
	public function add_column_to_database ( $ext_table , $column ) {
		global $wpdb_x;
		$full_table = $wpdb_x->get_ext_table_full_name ( $ext_table );
		if ( !DBX_DataAccess::db_table_exists ( $ext_table ) ) {
			throw new ErrorException ( "Trying to add a column to a database table that doesn't exist!." , DBX_ERROR_ADD_COL_NO_TABLE );
		} else if ( DBX_DataAccess::db_column_exists ( $ext_table , $column )) {
			throw new ErrorException ( "Trying to add a column to a database table where the row already exists!" , DBX_ERROR_ADD_COL_ALREADY_EXISTS );
		} else {
			$result = DBX_DataAccess::add_column_to_database ( $ext_table , $column );
			if ( $result ) return $result;
			else throw new ErrorException ( "Creating a new column in the {$ext_table} extension table failed in call to DBX_DataAccess::create_table()." , DBX_ERROR_ADD_COL_UNKNOWN_ERROR );
		}
	}
	
	
	/**
	* map_post_to_extension
	* 
	* maps a post type -- basic post or CPT -- to one or more extension tables
	*
	* @param string 	$post_type_name 	The string name of the post type being mapped
	* @param array 		$mapped_ext_tables	The array of extension tables that will provide storage to this post-type
	* @param boolean 	$ignore_existance_of_post_type This allows you to turn off the check for post-type existance; 
	*					this can be useful in cases where the plugin that sets these pos-types is initialised after this is called
	* @return boolean 	Returns true or false based on success of operations
	* @author Ken Snyder
	*/
	public function map_post_to_extension ($post_type_name , array $mapped_ext_tables , $ignore_existance_of_post_type = false ) {
		if ( !$ignore_existance_of_post_type && !post_type_exists ( $post_type_name )) {
			if (class_exists ('AppLogger') ) AppLogger::warning ( "tried to map '$post_type_name' post type to an extension table but $post_type_name is not a valid post type. Ignoring the mapping request." ); 
			return false;
		}
		if ( isset ( $this->mapping_tables [$post_type_name] ) ) {
			if (class_exists ('AppLogger') ) AppLogger::warning ( "$post_type_name was historically defined; now redefining." ); 
		}
		$this->mapping_tables [$post_type_name] = $mapped_ext_tables;
	}
	/**
	* get_post_meta
	* 
	* Mirrors the signature of the WP call of the same name and similarly returns a single meta attribute about the post
	* but unlike the default WP one this one looks for the meta data in the extension tables.
	* Note: WP's post.php has get_post_meta which proxies call to get_metadata() in meta.php
	*
	* @param int	$post_id 	The id of the post who's metadata is being looked for 
	* @param string	$meta_key	Optional. Metadata key. If not specified, retrieve all metadata for the specified post_id.
	* @param bool 	$single		Optional, default is true (opposite of WP default). If true, return only the first value of the specified meta_key. 
	* 							this parameter has no effect if meta_key is not specified.
	* @return string|array 		If there is a single value than a string is returned; an array is returned in the case of multiple values
	* @author Ken Snyder
	*/
	public function get_post_meta ( $post_id , $meta_key = "" , $single = true ) {
		global $wpdb_x;
		//if (class_exists ('AppLogger') ) AppLogger::log_call_stack ( "get_post_meta ( $post_id , $meta_key , $single )" ); 
		
		/*
		The goal of this function is to simply call the more generic get_metadata(). This function is more specific in that it 
		knows that this is a "post" (versus user, tax, or comment data). This additional "context" is important in normal WP
		because the unique key of the cache (and it's ability to effectively query the DB) is based on knowing:
		
				"meta_type" (post/user/comment) + "primary_key of transaction" (wp_posts.id in the case of post)
				
		This "uniqueness quotient", however, is mildly different when data is stored in extension tables as there is an abstraction
		between the transaction data (aka, post, user, comment) and the extension table. The goal in this case is to get to:
				
				"post type" (wp_posts.post_type) + "key" (DB column name or Attribute name stored JSON hash)
				
		This will provide uniqueness as long as aggregate of the extension tables (aka, Extension Set) have no overlapping column/attribute names. 
		This is assumed to be the case and will later be enforced systematically. For reference the relationship is:
		
				1 Post-Type => Extension Set (composed of a 1:M relationship between post-type and extension-table)
		
		A secondary goal is to key all method signatures identical to the Wordpress functions whose name they borrow from. While the 
		signature remains identical there will be a little (as little as possible) repurposing of the payloads that these fields carry. 
		These changes will be more about object substitution than semantic substitution though. For instance, "meta_type" refers explicitly
		to an enumerated set of values in WP (post, user, comment) but semantically it is the telling the function which underlying table to 
		look for the data in WP tables. We will keep this underlying semantic at the same time that we move outside the contraints of the 
		overly restrictive enum list implied in the default WP model. That means that $meta_type will be equivalent to "extension table" here.
		
		Furthermore, the $meta_key which was just any freeform string attribute in normal WordPress is now mildly more constrained
		(or at least "structured") as it is a pointer to a database column or a dictionary element in the JSON extension hash.
		*/				
		$post_type = get_post_type ( $post_id );
		$ext_tables = $this->get_extension_tables ( $post_type );
		return $this->get_metadata ( $ext_tables , $post_id , $meta_key , $single );
	}
		
	/**
	* get_metadata
	* 
	* Mirrors the signature and much of the code of the WP call by the same name but calls to caching functions
	* are localised to this object's methods rather than WP methods which don't know about the extension tables. 
	*
	* Functional Description: 
	* 	1. Loop through each Extension table:
	* 	a) look for the value in the cache and return if there an entry
	* 	b) query the database extension tables for values, store in cache (for future requests), and add to resultset
	* 	2. Return aggregated values array across all relevant extension tables
	*
	* Mapping:
	* 	$meta_type = array of extension tables (now renamed to $ext_tables)
	* 	$object_id = $post_id
	* 	$meta_key  = DB column or JSON attribute
	*
    * @param string $meta_type 	Type of object metadata is for (e.g., comment, post, or user)
    * @param int 	$object_id 	ID of the object metadata is for
    * @param string $meta_key 	Optional.  Metadata key.  If not specified, retrieve all metadata for
    * 							the specified object.
    * @param bool 	$single 	Optional, default is false.  If true, return only the first value of the
    * 							specified meta_key.  This parameter has no effect if meta_key is not specified.
    * @return string|array 		Single metadata value, or array of values
	* @author Ken Snyder
	*/
	public function get_metadata ($ext_tables , $object_id, $meta_key = '', $single = false) {
		if ( !$ext_tables )
			return false;
		// if meta_type passed in as a string, convert to a single item array 
		// so that we can universally treat as an array
		if ( !is_array ( $ext_tables ) ) $ext_tables = array ( $ext_tables );
		// do a simple check that object_id (aka, post-id) is properly formatted as an integer
		if ( !$object_id = absint($object_id) )
			return false;
		
		$response = array();

		foreach ( $ext_tables as $ext_table ) {
			if (class_exists ('AppLogger') ) AppLogger::log ( "Iterating through extension tables: $ext_table" ); 
			//$check = apply_filters( "get_ext_{$ext_table}_metadata", null, $object_id, $meta_key, $single );
			// TODO: bring below section back in once working
			// if ( null !== $check ) {
			// 	if ( $single && is_array( $check ) )
			// 		return $check[0];
			// 	else
			// 		return $check;
			// }
			
			// Try and get the extension table from cache
			$meta_cache = wp_cache_get( $object_id , WP_DBX_CACHE_GROUP . "_" . $ext_table );
			// If not found in cache, use update method to get from database (and place in cache for next time)
			if ( !$meta_cache ) {
				// ORIGINAL WP CALL: meta_cache = update_meta_cache( $meta_type, array( $object_id ) );
				if (class_exists ('AppLogger') ) AppLogger::log ( "not found in cache, calling update_meta_cache for $ext_table with object id $object_id." ); 
				$meta_cache = $this->update_meta_cache( $ext_table , array( $object_id ) );
			} else if (class_exists ('AppLogger') ) AppLogger::log ( "Results found in wp_cache__get: " . print_r ( $meta_cache , TRUE ) ); 

			$response[$ext_table] = $meta_cache;
			
			// TODO: evaluate whether this section should come back in 
			// if ( !$meta_key )
			// 	return $meta_cache;

			// if ( isset($meta_cache[$ext_table]) ) {
			// 	if ( $single )
			// 		return maybe_unserialize( $meta_cache[$meta_key][0] );
			// 	else
			// 		return array_map('maybe_unserialize', $meta_cache[$meta_key]);
			// }
			// 
			// if ($single)
			// 	return '';
			// else
			// 	return array();
			// 			
		}
		return $response;
	}
	
	/** Meant as a possible replacement for get_metadata that has no historical attachment to trying to hook into the WP meta system **/
	public function get_dbx_data ( $post_id , $column = false ) {
		$post_type = get_post_type ( $post_id );
		$ext_tables = $this->get_extension_tables ( $post_type );
		$meta = array();
		if (class_exists ('AppLogger') ) AppLogger::debug ( "Iterating through extension tables for post id {$post_id}\n" . print_r ( $ext_tables , TRUE ) ); 
		foreach ($ext_tables as $ext_table) {
			$meta_cache = wp_cache_get( $post_id , WP_DBX_CACHE_GROUP . "_" . $ext_table );
			if ( empty($meta_cache) ) {
				$meta_cache = $this->update_meta_cache( $ext_table, $post_id );
				$meta_cache = $meta_cache[$post_id]; // this is odd that it returns a differnt structure; may want to look into this
				if (class_exists ('AppLogger') ) AppLogger::debug ( "Post ID {$post_id} NOT found in cache. Using update_meta_cache() to retrieve:\n" . print_r($meta_cache,TRUE)); 
			} else {
				if (class_exists ('AppLogger') ) AppLogger::debug ( "Post ID {$post_id} found in cache; meta value returned: \n" . print_r($meta_cache,TRUE)); 
			}
			$meta = array_merge ( $meta , $meta_cache );
		}
		return $meta;
	}
	
	/**
	* flatten_meta
	* 
	* Functions like get_post_meta return as attribute lists broken up by extension table. 
	* this function removes the ext-table element and merges the attributes into a single set.
	* As there are three conflict columns -- id, post_id, and json_ext -- these need to be dealt 
	* with seperately:
	* 
	*
	* @param array $meta_array The hierarchical meta ( [ext-table][column][value] )
	* @return array 
	* @author Ken Snyder
	*/
	public function flatten_meta ( $meta_array ) {
		if ( !isset ( $meta_array ) || empty ($meta_array) || !is_array ( $meta_array ) ) {
			if (class_exists ('AppLogger') ) AppLogger::error ( "In function flatten_meta, the input parameter 'meta_array' was malformed. Existing" ); 
			return false;
		} else {
			$resultset = array();
			foreach ($meta_array as $ext_table) {
				foreach ( $ext_table as $col => $attr) {
					// TODO: if we're handling all three conditionals the same way then combine into one statement
					if ( $col === "json_ext" ) {
						$resultset[$col . '_' . array_keys($ext_table)] = $attr;
					} else if ( $col === "post_id" ) {
						$resultset[$col . '_' . array_keys($ext_table)] = $attr;
					} else if ( $col === "id" ) {
						$resultset[$col . '_' . array_keys($ext_table)] = $attr;	
					} else {
						$resultset[$col] = $attr;
					}
				}
			}
		}
		return $resultset;
	}

	/**
	* update_transaction
	* 
	* this function is not meant to mimic any base WP functionality but instead offers an efficient way to update a transaction (aka, wp_posts and any
	* extension tables that are associated to the post_type).
	*
	* @param class  		$CPT		A subclass of the the LG_CustomPostType class 
	* @param array  		$options	A hash of any additional options the caller wants to pass in 
	* @return void 						Will throw an exception if problems occur
	**/
	public function update_transaction ( $CPT , $options = array() ) {
		if ( ! $CPT instanceof LG_CustomPostType ) 
			throw new Exception ( 'Called update_transaction() but the variable passed in was not of type LG_CustomPostType');
		
		// if the base transaction in wp_posts has been modified and calling object isn't blocking then update the wp_post entry
		// if ( $CPT->is_table_updated ('wp_posts') && ! LG_Utility::exists_and_contains ($options , 'exclude' , 'wp_posts' ) ) {
			wp_update_post ( $CPT->table_as_array('wp_posts') );
		// }
		
		// now update the extension tables which have been modified (again checking for any exclusions sent in via the $options parameter)
		foreach ($CPT->get_extension_tables() as $ext_table) {
			if ( !$CPT->is_table_updated ($ext_table) || LG_Utility::exists_and_contains ($options, 'exclude' , $ext_table ))
				continue;
			$updated_hash = $CPT->get_updated ( $ext_table );
			$this->update_extension_table ( $ext_table , $CPT->wp_posts->ID, $updated_hash );
		}
	}
	
	/**
	* insert_transaction
	* 
	* this function is not meant to mimic any base WP functionality but instead offers an efficient way to insert a transaction (aka, wp_posts and any
	* extension tables that are associated to the post_type).
	*
	* @param class  		$CPT		A subclass of the the LG_CustomPostType class 
	* @param array  		$options	A hash of any additional options the caller wants to pass in 
	* @return void 						Will throw an exception if problems occur
	**/
	public function insert_transaction ( $CPT , $options = array() ) {		
		if ( ! $CPT instanceof LG_CustomPostType ) 
			throw new Exception ( 'Called update_transaction() but the variable passed in was not of type LG_CustomPostType');
		
		// for inserts it expected that at least SOME values in wp_posts will have been updated
		if ( !$CPT->is_table_updated ('wp_posts') ) 
			throw new Exception ( 'Trying to insert a new transaction but no fields in wp_posts are set.');
		else {
			$post_data = $CPT->table_as_array('wp_posts');
			$unsets = array ( 'ID' , 'post_date' ,'post_date_gmt' , 'post_modified', 'post_modified_gmt' , 'post_name' );
			foreach ( $unsets as $unset )
				unset ( $post_data [$unset] );
			\AppLogger::debug ( "PRE-INSERT: " . print_r ( $post_data , TRUE ) ); 
			$result_post_id = wp_insert_post ( $post_data );
			if ( ! $result_post_id > 0 )
				throw new Exception ( "Unsuccessful in creating a new post ({$post_data['post_title']})." );
		}
		
		// now update the extension tables which have been modified (again checking for any exclusions sent in via the $options parameter)
		foreach ($CPT->get_extension_tables() as $ext_table) {
			if ( !$CPT->is_table_updated ($ext_table) || LG_Utility::exists_and_contains ($options, 'exclude' , $ext_table ))
				continue;
			$updated_hash = $CPT->get_updated ( $ext_table );
			$this->update_extension_table ( $ext_table , $result_post_id, $updated_hash );
		}
		return $result_post_id;
	}
	
	
	/**
	* update_extension_table
	* 
	* Updates the database extension tables and then clears the wordpress cache for this entry
	*
	* @param string 	$ext_table		The extension table that is being updated 
	* @param array 		$updated_hash	An associate array of column names => column values; only columns that were updated should be displayed
	* @return void 						Will throw an exception if problems occur
	**/
	public function update_extension_table ( $ext_table , $post_id , $updated_hash ) {
		global $wpdb;
		$table_name = WP_DBX_PREFIX  . $ext_table;
		$cache_key = WP_DBX_CACHE_GROUP . "_" . $ext_table;
		
		if (class_exists ('AppLogger') ) AppLogger::debug ( "SQL: starting update_extension_table"  ); 
				
		// Branch based on whether an INSERT or UPDATE command is required
		$ext_table_row_exists = DBX_DataAccess::row_exists ( $ext_table , $post_id );
		if ( $ext_table_row_exists ){
			// UPDATE SQL
			$table_sql = "\nUPDATE $table_name \nSET ";
			$where_sql = "\nWHERE post_id = $post_id \n";
		} else {
			// INSERT SQL				
			$table_sql = "\nINSERT INTO $table_name \n\tSET post_id=$post_id , \n\t";
			$where_sql = "";
		}
		// Iterate through the columns and build SQL syntax
		$column_sql = array();
		foreach ($updated_hash as $name => $value) {
			if ( preg_match ( '/^json_ext_/' , $name) === 1  ) {
				$value = json_encode($value);
				//$value = base64_encode (serialize ( $value ));
			}
			array_push ($column_sql , "{$name} = '{$value}'" );
		}
		$column_sql = implode ( ",\n\t" , $column_sql );
		
		// Build the SQL Query 		
		$db_query = $table_sql . $column_sql . $where_sql;
		if (class_exists ('AppLogger') ) AppLogger::debug ( "SQL for UPDATE: \n{$db_query}\n" ); 
		// Fire WP Hook
		do_action( "update_{$ext_table}_ext_table", $post_id, $updated_hash );
		// Execute SQL Query
		$db_result = $wpdb->get_results ( $db_query );
		if ( $db_result ) 
			if (class_exists ('AppLogger') ) AppLogger::log ( "SQL INSERT/UPDATE was successful for id $post_id" );
		else
			if (class_exists ('AppLogger') ) AppLogger::error ( "SQL INSERT/UPDATE was not successful for id $post_id\n" . print_r ( $wpdb->debug() , TRUE ) );  
		do_action( "updated_{$ext_table}_ext_table", $post_id, $updated_hash );
			
		// Delete the cache entry
		wp_cache_delete( $post_id, $cache_key );
	}

	
	
	
	/**
	* update_post_meta
	* 
	* Mirrors WP function of same name but supports extension tables
	*
	* @param int 			$post_id 		The transaction ID. This is used directly and indirectly to build list of extension tables.
	* @param string/array 	$col_names		The column name that is being updated. This can be a single string or an array. 
	* @param string/array 	$meta_values	The value (or values) associated to the col_name(s)	
	* @return boolean						True or false based on success of operation
	* @author Ken Snyder
	*/
	public function update_post_meta ($post_id , $col_names , $meta_values , $prev_value = false) {
		global $wpdb;
		if (class_exists ('AppLogger') ) AppLogger::log_call_stack ( __FUNCTION__ . " ($post_id, col_names , meta_value )" ); 
		// Get names and values into arrays
		if ( !is_array ( $col_names ) ) $col_names = array ( $col_names );
		if ( !is_array ( $meta_values ) ) $meta_values = array ( $meta_values );
		if ( count ( $col_names ) !== count ( $meta_values ) ) {
			$cols = count ( $col_names );
			$vals = count ( $meta_values );
			if (class_exists ('AppLogger') ) AppLogger::error ( "the number column ($cols) and meta_values ($vals) did not match! Exiting call. " ); 
			return false;
		}
		// Get the post-type based on ID
		$post_type = get_post_type ( $post_id );
		// Get a hash that can be used to lookup the extension table based on key name
		$key_list = $this->get_post_type_custom_keys ( $post_id );
		if (class_exists ('AppLogger') ) AppLogger::debug ( "KEY LIST: \n" . print_r ( $key_list , TRUE ) ); 
		// Organise names and values into extension tables
		$names_by_ext = array ();
		$values_by_ext = array ();
		$i = 0;
		foreach ($col_names as $col) {
			// $ext = $this->lookup_extension_table ( $post_type, $col );
			$ext = false;
			if (!isset ( $key_list[ $col ] ) ) {
				if (class_exists ('AppLogger') ) AppLogger::warning ( __FUNCTION__ . " was unable to find col $col's extension table in key_list hash for post-type '$post_type' \n" . print_r ( $key_list , TRUE ) ); 
				$i++;
				continue;
			} else {
				$ext = $key_list[ $col ]['ext-table'];
				if ( !isset ( $names_by_ext [$ext] ) ) $names_by_ext [$ext] = array();
				if ( !isset ( $values_by_ext [$ext] ) ) $values_by_ext [$ext] = array();
				array_push( $names_by_ext [$ext] , $col );
				array_push( $values_by_ext [$ext] , $meta_values [$i] );
				$i++;
			}
		}
		
		// Loop through each extension table and perform DB update
		$i = 0;
		foreach ($names_by_ext as $ext_table => $key_names) {
			// Setup some convenience variables
			$table = WP_DBX_PREFIX  . $ext_table; // db table
			$cache_key = WP_DBX_CACHE_GROUP . "_" . $ext_table;
			
			$key_values = $values_by_ext [$ext_table];
			$old_values_by_ext = wp_cache_get ( $post_id , $cache_key );
			
			// Reset switches
			$j = 0; // reset counter
			$values_to_update = false; // boolean on whether anything has changed
			
			// Branch based on whether an INSERT or UPDATE command is required
			$ext_table_row_exists = DBX_DataAccess::row_exists ( $ext_table , $post_id );
			if ( $ext_table_row_exists ){
				// UPDATE SQL
				$table_sql = "\nUPDATE $table \nSET ";
				$where_sql = "\nWHERE post_id = $post_id \n";
			} else {
				// INSERT SQL				
				$table_sql = "\nINSERT INTO $table \n\tSET post_id=$post_id , \n\t";
				$where_sql = "";
			}
			
			// Iterate through the columns looking for changes and adding to an array where there is
			$columns = array();
			foreach ($key_names as $name) {
				$value = $key_values[$j];
				if ( $ext_table_row_exists && $value !== $old_values_by_ext[$name] ) {
					if (class_exists ('AppLogger') ) AppLogger::log ( "Values of $name changed from $value to " . $old_values_by_ext[$name] ); 
					array_push ($columns , "$name = '$value' " );
					$values_to_update = true;
				} else if ( !$ext_table_row_exists && !empty ($value) ){
					if (class_exists ('AppLogger') ) AppLogger::log ( "Values for $name will be inserted as there was no prior row in '$ext_table' ext table " ); 
					array_push ($columns , "$name = '$value' " );
					$values_to_update = true;					
				} else { 
					if (class_exists ('AppLogger') ) AppLogger::log ( "Not updating $name as the value hasn't changed ($value)" ); 
				}
				$j++;
			}
			$column_sql = implode ( ",\n\t" , $columns);
			
			// check if there is anything to update on this table
			if ( $values_to_update ) {	
				$db_query = $wpdb->prepare ( $table_sql . $column_sql . $where_sql );
				if (class_exists ('AppLogger') ) AppLogger::debug ( "About to execute SQL: $db_query" );
				// Update to Database 
				do_action( "update_{$post_type}_ext", $post_id, $col_names, $meta_values , $old_values_by_ext );
				$db_result = $wpdb->get_results ( $db_query ); // RUN QUERY
				if ( $db_result ) 
					if (class_exists ('AppLogger') ) AppLogger::log ( "SQL INSERT/UPDATE was successful for id $post_id" );
				else
					if (class_exists ('AppLogger') ) AppLogger::error ( "SQL INSERT/UPDATE was not successful for id $post_id\n" . print_r ( $wpdb->debug() , TRUE ) );  
				do_action( "updated_{$post_type}_ext", $post_id, $col_names, $meta_values , $old_values_by_ext );
			
				// Delete the cache entry
				wp_cache_delete($post_id, $cache_key);
			
				if (class_exists ('AppLogger') ) AppLogger::log ( "updated $ext_table extension table for post ID $post_id" ); 
			} else {
				if (class_exists ('AppLogger') ) AppLogger::log ( "extension table '$ext_table' doesn't need to be updated as no values have changed. Skipping." ); 
			}
			$i++;
		}
		
		do_action( 'updated_postext', 'extension' , $post_id, $col_names , $meta_values ); // provides a better action hook but plugins won't know about it
		do_action( 'updated_postmeta', 'extension' , $post_id, $col_names , $meta_values ); // TODO: make sure this is reasonable; the last two values are arrays whereas I think the original spec had strings so this might be a problem.
		return true;
	}
	
	/**
	* update_extension_tables
	* 
	* Meant as a possible replacement to update_post_meta. Called by admin interface when an update is made.
	* NAMING IS TERRIBLE AS THERE IS ANOTHER FUNCTION CALLED "update_extension_table"!
	*
	* @param int 		$post_id 			The post_id for the transaction
	* @param array 		$changed_values		An associative array of [name]=>[value]
	* @param array 		$nullified_values	A simple list of column names that have been nullified
	* @return null  						
	* @author Ken Snyder
	*/
	public function update_extension_tables ( $post_id , array $changed_values , array $nullified_values = array() ) {
		// FIXME: rename either this function or the function with the singular name (or ideally remove one if that's appropriate)
		global $wpdb;
		// Get the post-type based on ID
		$post_type = get_post_type ( $post_id );
		// Get a flat dictionary of all keys associated with the give ID's post-type; used to lookup the extension table based on key name
		$key_list = $this->get_post_type_custom_keys ( $post_id );
		// Extension tables variable used to store tables that need updating/inserting
		$ext_tables = array();
		// identify tables that need updating based on a value being set
		foreach ($changed_values as $attribute => $value) {
			$ext_table = $this->get_extension_tables ($post_type, $attribute);
			// it is assumed here that only ONE extension table will come back (or none) as each table has a uniqueness constraint on column names
			// although must a similarly named column could exist across ext_tables associated to a post-type the design intent it NOT to do this
			// as a result we simply take the FIRST array element to represent the result
			$update_table = $ext_table[0];
			if ( !empty($ext_table) )
				$ext_tables[$update_table][$attribute] = $value;
		}
		// identify tables that need updating based on a value being cleared
		foreach ($nullified_values as $attribute) {
			// $ext_table = $key_list[$attribute]['ext-table'];
			$ext_table = $this->get_extension_tables ($post_type, $attribute);
			// the same assumption about uniqueness as applied above so using the first array element as the table
			$update_table = $ext_table[0];
			if ( !empty($ext_table) )
				$ext_tables[$update_table][$attribute] = null;
		}
		if (class_exists ('AppLogger') ) AppLogger::debug ( "EXT_TABLES: " . print_r ( $ext_tables , TRUE ) ); 

		// Now iterate through the extension tables that have changes in them
		foreach (array_keys($ext_tables) as $ext_table) {
			if (class_exists ('AppLogger') ) AppLogger::debug ( "SQL-PRECURSOR: " . print_r ( $ext_tables , TRUE ) ); 
			// Setup some convenience variables
			$table = WP_DBX_PREFIX  . $ext_table; 					// db table
			$cache_key = WP_DBX_CACHE_GROUP . "_" . $ext_table;		// WP cache key
			// Determine whether this is an insert or update operation
			$is_an_update = DBX_DataAccess::row_exists ( $ext_table , $post_id );
			// Build the query
			$table_sql = $where_sql = "";
			$column_sql = array();
			if ( $is_an_update ){
				// UPDATE SQL
				$table_sql = "\nUPDATE $table \nSET ";
				$where_sql = "\nWHERE post_id = $post_id \n";
			} else {
				// INSERT SQL
				$table_sql = "\nINSERT INTO $table \n\tSET post_id=$post_id , \n\t";
				$where_sql = "";
			}
			// iterate through columns and build $column_sql
			foreach ($ext_tables[$ext_table] as $name => $value) {
				if ( $value === null ) 
					array_push ($column_sql , "$name = NULL " );
				else 
					array_push ($column_sql , "$name = '$value' " );
			}
			$column_sql_string = implode ( ",\n\t" , $column_sql);
			// assuming at least one attribute was changed then execute SQL
			if (class_exists ('AppLogger') ) AppLogger::debug ( "SQL: {$table_sql}\n{$column_sql_string}\n{$where_sql}\n" ); 
			if ( count($column_sql) > 0 ) {
				wp_cache_delete($post_id, $cache_key);
				$db_query = ( $table_sql . $column_sql_string . $where_sql );
				if (class_exists ('AppLogger') ) AppLogger::debug ( "SQL (update ext tables): \n" . print_r ( $db_query , TRUE ) ); 
				do_action( "update_{$post_type}_ext", $post_id );
				$db_result = $wpdb->get_results ( $db_query ); // RUN QUERY
				do_action( "updated_{$post_type}_ext", $post_id );
			}
		}
		do_action( 'updated_extension_tables', $post_id, array_keys($ext_tables) ); 
	}
	
	
	/**
	* update_meta_cache
	* 
	* updates the metadata cache for specified objects that are stored in the database extension tables
	* note: this largely repeats the code of the default WP function (meta.php) but changes DB queries to collect data from the 
	* right place. 
	*
	* @param string $meta_type Type of object metadata this is for (e.g., comment, post, or user)
	* @return mixed  Metadata cache for the speicified objects, or false on failure
	* @author Ken Snyder
	*/
	public function update_meta_cache ( $ext_table , $object_ids ) {
		global $wpdb;
		if (class_exists ('AppLogger') ) AppLogger::log_call_stack ( __FUNCTION__ . "( $ext_table , $object_ids ) " );
		if ( empty( $ext_table ) || empty( $object_ids ) )
			return false;
		if ( !is_array($object_ids) ) {
			$object_ids = preg_replace('|[^0-9,]|', '', $object_ids);
			$object_ids = explode(',', $object_ids);
		}
		$object_ids = array_map('intval', $object_ids);
		$table = WP_DBX_PREFIX  . $ext_table; // db table
		$cache_key = WP_DBX_CACHE_GROUP . "_" . $ext_table;
		$non_cached_ids = array();
		$cache = array();
		foreach ( $object_ids as $id ) {
			$cached_object = wp_cache_get( $id, $cache_key );
			if ( false === $cached_object )
				array_push ( $non_cached_ids , $id );
			else 
				$cache[$id] = $cached_object;
		}
		if ( empty( $non_cached_ids ) ) {
			if (class_exists ('AppLogger') ) AppLogger::log ( "All ID's appear to be cached so returning cache. Cache has a count of " . count($cache) );
			return $cache;
		}
		
		// Get non-cached data from database
		$id_list = join(',', $non_cached_ids);
		$query_string = $wpdb->prepare ("SELECT * FROM $table WHERE post_id IN (%s)", $id_list );
		if (class_exists ('AppLogger') ) AppLogger::debug ( "Query Prepared: $query_string" ); 
		$meta_list = $wpdb->get_results ( $query_string , ARRAY_A );
		
		if ( !empty($meta_list) ) {			
			// organise results grouped by the post_id 
			foreach ($meta_list as $row) {
				$the_id = $row ['post_id'];
				if ( !isset ( $cache[$the_id] ) ) $cache[$the_id] = array();
				// array_push ( $cache[$the_id] , $row );
				$cache[$the_id] = $row;
			}
		}
		
		// iterate through the ids that had been out-of-cache; they should have values now so add them
		foreach ( $non_cached_ids as $id ) {
			if ( ! isset($cache[$id]) )
				$cache[$id] = array();	// TODO: understand why this is important
			wp_cache_add( $id, $cache[$id], $cache_key );
		}

		return $cache;
	} // end update_meta_cache
	
	
	
	/**
	* get_post_custom
	* 
	* This mimics the signature of the WP function of the same name and is designed to return all custom fields of a particular post-type
	* with the difference being that it is looking up these custom fields in the database extension tables(s) rather than wp_postmeta.
	*
	* @param int $post_id The post id that is being worked on
	* @return array Associative array of fields and values: $result['field-name'][array-index][array-value]
	* @author Ken Snyder
	*/
	public function get_post_custom ($post_id ) {
		$post_type = get_post_type ( $post_id );
		// passing through to more specific signature; this base signature is easier and backwards compatible but if you already
		// explicitly know the post-type you are coding for you can call this method directly and save yourself an additional call
		return $this->get_post_type_custom ( $post_id , $post_type );
	}
	
	/**
	* get_post_type_custom
	* 
	* Same functional goal of get_post_custom() but this method has the benefit of knowing the explicit post_type information. 
	* Implementation for both methods is done here.
	*
	* @param int $post_id The explicit post record that is being worked on
	* @param string $post_type	The slug/name of the custom post type being worked on
	* @return array  Associative array of fields and values: $result['field-name'][array-index][array-value]
	* @author Ken Snyder
	*/
	public function get_post_type_custom ( $post_id , $post_type ) {
		$my_keys = $this->get_keys ( $post_type );
		foreach ($my_keys as $key => $value) {
			$this->get_post_meta ( $post_id , $key );
		}
		return $my_keys;
	}
	
	/**
	* get_post_type_custom_keys
	* 
	* Get the keys back for a specific post-type. The main lookup is on the extension_tables based on $post_type. 
	* The $post_id is just there to keep the signature the same as the WP function. For convenience just use the 
	* get_keys() function below.
	*
	* @param int $post_id 		The unique identifier of the current transaction
	* @param string $post_type	The slug/name of the post type
	* @return array|boolean		Will return an array of key values if successful, otherwise will return 'false'
	* @author Ken Snyder
	*/
	public function get_post_type_custom_keys ( $post_id , $post_type = false , $show_hidden = true , $show_attributes = true ) {
		if (class_exists ('AppLogger') ) AppLogger::log_call_stack ( __FUNCTION__ . " ( $post_id, $post_type )" ); 	
		
		if (! $post_id ) $post_id = 0;
		if (! $post_type ) $post_type = get_post_type ( $post_id );
		
		$my_ext_tables = $this->get_extension_tables ( $post_type );
		$key_list = array ();
		foreach ($my_ext_tables as $ext_table) {
			$columns = $this->get_columns_meta ( $ext_table , null , !$show_hidden);
			if ( $show_attributes )
				$attributes = $this->get_attributes_meta ( $ext_table );
			else 
				$attributes = array();
			$key_list = array_merge ( $key_list , $columns , $attributes );
		}
		return $key_list;
	}
	
	public function get_keys ( $post_type ) {
		return $this->get_post_type_custom_keys ( 999 , $post_type );
	}
	
	public function get_mapping_relationships () {
		return $this->mapping_tables;
	}
	/**
	* get_base_id
	* 
	* returns the ID but if it is a "revision" then it returns the root/base ID associated with that
	* chain of posts. This base ID is where extension data is stored. 
	*
	* @param int $post_id The current transaction
	* @return int The base id (which might be the same as the post_id but won't be in the case of revisions)
	* @author Ken Snyder
	*/
	public function get_base_id ( $post_id ) {
		$post_info = get_post ( $post_id );
		if ( $post_info->post_type === "revision" ) {
			list ( $root_post_id , $revision , $revision_number ) = split ( '-' , $post_info->post_name );
			if (class_exists ('AppLogger') ) AppLogger::log ( "Post ID ($post_id) was a revision; base ID is '$root_post_id'" ); 
			return $root_post_id;
		} else {
			if (class_exists ('AppLogger') ) AppLogger::log ( "Post ID ($post_id) is not revision (" . $post_info->post_type .  ") so returning it as the valid base ID" );
			return $post_id;
		}
	}
	
	/**
	* get_extension_tables
	* 
	* Lists the extension tables. This can be scoped in three ways:
	* 	a) if called with no parameters than ALL extension tables are returned
	*	b) if a post_type is passed in than it will be constrained to only those associated to that post_type
	*   c) if an attribute is passed in than it will only return extension tables that have this ID
	*
	* @param 	string 	$post_type 	String name/slug for post-type (optional param)
	* @param 	string 	$attribute 	the column name to look for (optional param)
	* @return 	array 				Simple list of extension tables (no ornamentation of attributes)
	* @author 	Ken Snyder
	*/
	public function get_extension_tables ( $post_type = false , $attribute = false  ) {
		$ext_list = array();		
		
		if ( $post_type && !$attribute ) {
			if ( isset ($this->mapping_tables [ $post_type ]) ) 
				$ext_list = $this->mapping_tables [ $post_type ];
		} else if ( $post_type && $attribute ) {
			foreach ( $this->mapping_tables[$post_type] as $map_tbl ) {
				if ( $this->column_exists($map_tbl,$attribute) )
					array_push( $ext_list , $map_tbl);
			}
		} else {
			foreach ( $this->extension_tables as $ext_table => $attr) {
				if ( !$attribute || $this->column_exists($ext_table,$attribute) )
					array_push ( $ext_list , $ext_table );
			}
		}
		
		return $ext_list;
	}
	
	/**
	* get_transactional_extension_tables
	* 
	* lists all extension tables associated with transactional CPT's. 
	*
	* @return array  
	* @author Ken Snyder
	*/
	public function get_transactional_extension_tables () {
		global $CustomTypes;
		$transactional_post_types = $CustomTypes->get_transactional_post_types();
		$ext_tables = array();
		// iterate through post-types and add a unique list of extension tables for this set of post-types
		foreach ($transactional_post_types as $post_type) {
			$ext_tables = ___::union($ext_tables,$this->get_extension_tables($post_type));
		}
		return $ext_tables;
	}
	
	
	/**
	* get_ext_table_full_name 
	* 
	* returns the full database table name for an extension table 
	*
	* @param Variable_Type Param 1 Variable Description
	* @return Return_Type Return Description
	* @author Ken Snyder
	*/
	public function get_ext_table_full_name  ( $ext_table ) {
		if ( $this->ext_table_exists ( $ext_table ) )
			return WP_DBX_PREFIX . $ext_table;	
		else 
			return false;
	}
	
	
	/**
	* get_post_types_extended
	* 
	* Lists the post types that are extended to one or more extension tables
	*
	* @param  void
	* @return array Simple list of extension tables (no ornamentation of attributes)
	* @author Ken Snyder
	*/
	public function get_post_types_extended () { 
		if (class_exists ('AppLogger') ) AppLogger::log_call_stack (); 
		$post_types = array();
		foreach ($this->mapping_tables as $cpt => $attr) {
			array_push ( $post_types , $cpt );
		}
		return $post_types;
	}
	
	
	/**
	* lookup_extension_table
	* 
	* Based on a post_id and column name, this function tells you the underlying extension table it came from
	*
	* @param string $post_type 	The name/slug for post-type (aka, post, activity, etc.)
	* @param string $col_name	Name of the column being looked up
	* @return string 			The extension table name
	* @author Ken Snyder
	*/
	public function lookup_extension_table ( $post_type , $col_name ) {
		if ( empty ( $post_type ) || empty ( $col_name ) ) {
			if (class_exists ('AppLogger') ) AppLogger::error ( "input parameters were malformed ( $post_type , $col_name ) in " . __FUNCTION__ ); 
			return false;
		}
		if ( !isset ( $this->mapping_tables [ $post_type ] ) ) {
			if (class_exists ('AppLogger') ) AppLogger::error ( "'$post_type' is not mapped to any extension tables" ); 
			return false;
		}
		foreach ($this->get_extension_tables($post_type) as $ext_table) {
			if ( $this->get_columns_meta ($ext_table , $col_name) )
				return $ext_table;
		}		
		return false; // no match was made
	}
	
	public function get_column_meta_from_post_type ( $post_type ) {
		$ext_tables = $this->get_extension_tables ( $post_type );
		if ( empty($ext_tables) )$meta = array();
		foreach ( $ext_tables as $ext_table ) {
			$meta [ $ext_table ] = $this->get_columns_meta ( $ext_table );
		}
		return $meta;
	}
	
	
	/**
	* get_columns_meta
	* 
	* gets all the columns (and associated attributes) of a given extension table; optionally restricts list ot a single column
	*
	* @param string $ext_table	The name of the extension table
	* @return hash Returns an associative array of columns with attributes of format: $return [column-name][attributes][values]
	* @author Ken Snyder
	*/
	public function get_columns_meta ( $ext_table , $col_name = null , $ignore_hidden_field = false) {
		$column_list = array ();
		if ( isset($this->extension_tables [$ext_table]['columns']) ) {
			// if asking for only a single column then ...
			if ( $col_name != null ) {
				if ( isset($this->extension_tables [$ext_table]['columns'][ $col_name ]))
					return $this->extension_tables [$ext_table]['columns'][ $col_name ];
				else {
					if (class_exists ('AppLogger') ) AppLogger::log ( "Column '$col_name' doesn't exist in $ext_table extension table.");
					return false; 
				}
			}	
			// else go through each column and optionally filter out hidden keys
			else {
				foreach ( $this->extension_tables [$ext_table]['columns']  as $col => $attr) {
					if ( $ignore_hidden_field && $this->is_hidden_field ( $ext_table , $col ) ) {
						if (class_exists ('AppLogger') ) AppLogger::log ( "Ignore hidden field $ext_table.$col in get_columns_meta" ); 
						continue;
					} else {
						$column_list [ $col ] = $attr;
					}
				}
			}
			return $column_list;
		} else return array();
	}
	
	// CONVENIENCE FUNCTIONS
	public function get_col_name ($ext_table, $col_name) {
		$meta = $this->get_columns_meta ($ext_table, $col_name);
		return $meta['name'];
	}
	public function get_col_uom ($ext_table, $col_name) {
		$meta = $this->get_columns_meta ($ext_table, $col_name);
		if (isset($meta['uom'])) return $meta['uom'];
		else return "";
	}
	
	/**
	* get_columns
	* 
	* gets a list of the columns of a particular extension table (but without any meta information)
	*
	* @param string  	$ext_table  	the extension table which you want the column list for
	* @return array 					a list of columns for a particular extension table
	* @author Ken Snyder
	*/
	function get_columns ( $ext_table ) {
		if ( !isset($this->extension_tables[$ext_table]) ) {
			throw new Exception ("Call to DBX's get_columns() asking for invalid extension table: {$ext_table}");
		} else {
			return array_keys ($this->extension_tables[$ext_table]['columns']);
		}
	}
		
	public function get_column_ddl ( $ext_table , $column ) {
		$column_meta = $this->get_columns_meta ( $ext_table , $column );
		$response_obj = new stdClass();
		list ( $type , $size ) = DBX_UtilityFunctions::seperate_type_and_size ( $column_meta ['type']);
		// if ( !$size ) $size = DBX_DataAccess::default_size_by_type ( $type );
		if ( $size )
			$response_obj->ddl = "{$type}({$size})";
		else 
			$response_obj->ddl = "{$type} ";
		$response_obj->type = $type;
		$response_obj->size = $size;
		if ( isset ($column_meta ['unsigned']) && $column_meta ['unsigned'] == 'unsigned' ) {
			$response_obj->unsigned = true;
			$response_obj->ddl .= " UNSIGNED";
		} else { $response_obj->unsigned = false; }
		if ( isset ($column_meta ['nullable']) && $column_meta ['nullable'] == 'NOT NULL' ) {
			$response_obj->nullable = false;
			$response_obj->ddl .= " NOT NULLABLE";			
		} else { $response_obj->nullable = true; }
		if ( isset ($column_meta ['pk'] ) && $column_meta ['pk'] == 'true' ) {
			$response_obj->pk = true;
			$response_obj->ddl .= " PRIMARY KEY";
		} else { $response_obj->pk = false; }
		if ( isset ($column_meta ['fk'] ) && $column_meta ['fk'] == 'true' ) {
			$response_obj->fk = true;
			// $response_obj->ddl .= " FOREIGN KEY";
		} else { $response_obj->fk = false; }
		
		return $response_obj;
	}
	
	/**
	* get_attributes_meta
	* 
	* list all the attributes in the json extension (and associated attributes) of a given extension table 
	*
	* @param string $ext_table	The name of the extension table
	* @return hash Returns an associative array of columns with attributes of format: $return [column-name][attributes][values]
	* @author Ken Snyder
	*/
	public function get_attributes_meta ( $ext_table , $attr_name = null ) {
		if ( isset($this->extension_tables [$ext_table]['attributes']['dictionary'][$attr_name]) )
			 return $this->extension_tables [$ext_table]['attributes']['dictionary'][$attr_name];
		else if ( isset($this->extension_tables [$ext_table]['attributes']['dictionary']) ) 
			return $this->extension_tables [$ext_table]['attributes']['dictionary'];
		else return array();
	}
	
	
	/**
	* add_grouping
	* 
	* creates a grouping object and passes back to calling function to operate on. It also keeps a reference to object so that the groupings can themselves be grouped.
	*
	* @param string  $ext_table  the extension table that this grouping is operating on
	* @return object  returns an instance of the DBX_FieldGrouping object
	* @author Ken Snyder
	*/
	public function add_grouping ( $ext_table , $slug_name , $name = false ) {
		$object = new DBX_FieldGrouping ( $ext_table , $slug_name , $name );
		if ( !$object ) {
			if (class_exists ('AppLogger') ) AppLogger::error ( "Failed to create a DBX_FieldGrouping object on '$ext_table' extension table." ); 
			return false;
		}
		$this->grouping_objects[$ext_table][$slug_name] = $object;
		return $object;
	}
	
	/**
	* get_groupings
	* 
	* sends back a simple array list of groupings associated with a given extension table 
	*
	* @param string		$ext_table	The name of the extension table in question
	* @return array 				A simple list of grouping names (slug names)
	* @author Ken Snyder
	*/
	public function get_groupings ( $ext_table ) {
		if ( !isset ( $this->grouping_objects[$ext_table] )) return array();
		else return $this->grouping_objects[$ext_table];
	}
	
	/**
	* get_grouping_fields
	* 
	* return the list of fields that a particular group has
	*
	* @param string  	$ext_table  	The extension table the grouping is part of
	* @param string		$group_name		The name of the group
	* @return array  					the list of fields associated with the group
	* @author Ken Snyder
	*/
	public function get_grouping_fields ( $ext_table , $group_name ) {
		if ( !isset ( $this->grouping_objects[$ext_table][$group_name] )) return array();
		else return $this->grouping_objects[$ext_table][$group_name]->get_fields();
	}
	
	/**
	* get_grouping_fields_by_post_type method
	* 
	* return the array of a particular grouping field for a post type (aka, across ext_tables). This is most likely to be
	* used in cases like "key-fields" and "ignore-fields" where there is a common group name across ext_tables.
	*
	* @param string		$post_type		The post type that you are looking for fields from
	* @param string		$group_name		The name of the group 
	* @return array 	$key_fields		array of "key fields" or "false" if the extension table doesn't exist
	* @author Ken Snyder
	*/
	public function get_grouping_fields_by_post_type ( $post_type , $group_name ) {
		$ext_tables = $this->get_extension_tables ( $post_type );
		if ( $ext_tables ) {
			$group_fields = array();
			foreach ($ext_tables as $ext_table) {
				$group_fields = array_merge ( $group_fields , $this->get_grouping_fields ( $ext_table , $group_name ));
			}
			return $group_fields;
		}
		else {
			if (class_exists ('AppLogger') ) AppLogger::error ( "Tried to get grouping fields for post type '{$post_type}' but no extension tables came back" );
			return array(); 
		}
	}
	
	public function get_key_fields_by_post_type ( $post_type ) {
		return $this->get_grouping_fields_by_post_type ( $post_type , 'key-fields' );
	}
	
	public function get_ignore_fields_by_post_type ( $post_type ) {
		return $this->get_grouping_fields_by_post_type ( $post_type , 'ignore-fields' );
	}
	
	/**
	* set_key_fields
	* 
	* Allow the specification of certain fields in an extension table which are considered "key". This attribute can be filtered on in the admin screens.
	*
	* @access public
	* @param string  		$ext_table	 	the extension table the key fields refer to
	* @param string/array  	$key_fields 	an array of fields
	* @return void 
	* @author Ken Snyder
	*/
	public function set_key_fields ( $ext_table , $key_fields ) {
		if ( !is_array ($key_fields) ) $key_fields = array ( $key_fields );

		$key_fields_obj = $this->add_grouping ( $ext_table , 'key-fields' , 'Key Fields' );
		$key_fields_obj->add_fields ( $key_fields );
	}
	public function set_ignore_fields ( $ext_table , $key_fields ) {
		if ( !is_array ($key_fields) ) $key_fields = array ( $key_fields );

		$key_fields_obj = $this->add_grouping ( $ext_table , 'ignore-fields' , 'Ignore Fields' );
		$key_fields_obj->add_fields ( $key_fields );
	}
	
	/**
	* get_key_fields method
	* 
	* return the array of key fields
	*
	* @access public
	* @param string		$ext_table		The extension table these key fields are in relation to 
	* @return array 	$key_fields		array of "key fields" or "false" if the extension table doesn't exist
	* @author Ken Snyder
	*/
	public function get_key_fields ( $ext_table ) {
		if ( ! $this->ext_table_exists( $ext_table ) ) {
			if (class_exists ('AppLogger') ) AppLogger::warning ( "Attempt to get key fields for non-existant extension table ({$ext_table})." ); 
			return false;
		} else {
			return $this->get_grouping_fields ( $ext_table , 'key-fields' );
		}
	}
	

	
	/**
	* is_key_field
	* 
	* returns true or false based on whether the specified field is consider a "key field"
	*
	* @param string 	$ext_table 		The extension table that this field is being looked for
	* @param string		$column_name 	The column that is being checked
	* @return boolean					True or false based on whether the field is a key field
	* @author Ken Snyder
	*/
	public function is_key_field ($ext_table , $column_name ) {
		if ( ! $this->ext_table_exists( $ext_table ) ) {
			if (class_exists ('AppLogger') ) AppLogger::warning ( "Attempt to test for key fields for non-existant extension table ({$ext_table})." ); 
			return false;
		} else {
			return in_array ( $column_name , $this->get_grouping_fields ($ext_table , 'key-fields') );
		}
	}
	
	/**
	* create_abstraction_map
	* 
	* Object factory to create a abstraction map which can define abstract data structures that have a defined relationship at the CPT level
	*
	* @param string 		$name		The name of the abstraction 
	* @param type 		$variable		description
	* @return type	$variable		description
	**/
	public function create_abstraction_map ( $name , $defaults = false ) {
		$object = new DBX_AbstractionMap ( $name , $defaults );
		$this->abstraction_maps[$name] = $object;
		return $object;
	}
	
	public function list_abstraction_maps ( $post_type = false ) {
		if ( !$post_type )
			return array_keys($this->abstraction_maps);
		else { 
			$map_list = array ();
			foreach ( $this->abstraction_maps as $map ) {
				if ( $map->has_override ( $post_type ) ) array_push ( $map_list , $map->name );
			}
			return $map_list;
		}
	}
	
	public function get_abstraction_map ( $name ) {
		if ( isset ( $this->abstraction_maps[$name] )) return $this->abstraction_maps[$name];
		else return false;
	}
	
} // END class 



	
/* END OF FILE */
