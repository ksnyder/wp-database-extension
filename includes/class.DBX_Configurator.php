<?php
/**
* @package database-extension-tables
**/

define ( 'WP_DBX_UOM_TABLE_POSTFIX' , '_uom');
define ( 'WP_DBX_UOM_COL_TYPE' , 'varchar(15)');

/**
* DBX_Configurator
*
* An object that helps configure DBX extension tables. It is returned by the DBX_TableExtension->add_extension_table method and should be used 
* as the primary method of configuring DBX column-level data.
*
* @package database-extension-tables
* @author Ken Snyder
*/
class DBX_Configurator {
	
	// Instance variables
	// ------------------
	/** holds the name of the extension table being operated on **/
	protected $ext_table;
	/** a pointer to the primary DBX state mechanism for column and table information **/
	protected $ptr_ext_tables;
	/** the only part of the "ext_tables" array that this class really needs to operated on is that part associated to the particular table an instance is reponsible for. This variable pairs down the scope to just the relevant scope **/
	protected $ptr_columns;
	/** pairs back the scope of ptr_ext_tables to just the "constraints" part of the larger array **/
	protected $ptr_constraints;
	
	public function __construct ( $ext_table , &$ptr_ext_tables ) {
		$this->ptr_extension_tables = $ptr_ext_tables;
		$this->ext_table = $ext_table;
		$this->ptr_columns = &$ptr_ext_tables[$ext_table]['columns'];
		$this->constraints = &$ptr_ext_tables[$ext_table]['constraints'];
	}
	
	/**
	 * base_add
	 *
	 * private method that adds a majority of the structure
	 *
	 * @author Ken Snyder
	*/
	private function base_add ( &$col_ref, $column_slug, $column_name , $column_type, $desc = "", $options = array() ) {
		// first add in the "options" array
		$col_ref = $options;
		// set some base meta attributes
		$col_ref['id'] = $column_slug; // not strictly necessary but makes access easier
		$col_ref['key-type'] = 'db-column';
		$col_ref['ext-table'] = $this->ext_table;
		$col_ref['display-type'] = $this->get_default_display_type ( $column_name );
		// now set the marker information coming from the incoming parameters
		$col_ref['name'] = $column_name;
		$col_ref['desc'] = $desc;
		$col_ref['type'] = $column_type;
		// Make all integers -- by default -- unsigned
		if ( preg_match ( '/.*int.*/' , $col_ref['type']) > 0 ) {
			$this->add_unsigned_constraint($this->ext_table,array($column_name));
		}
	}
	
	/**
	* add_marker
	*
	* Allows the addition of "markers" to a dbx table. Markers are columns that are intended to capture 
	* a measurement of some sort. They are accompanied by a UOM column that specifies what measurement system
	* was used for the marker. If the marker is static (aka, always the same UOM) then use add_static_marker instead
	*
	* @author Ken Snyder
	*/
	public function add_marker ( $marker , $column_type , $uom_context , $desc = "" , $options = array() ) {
		$column_name;
		$column_slug;
		if ( !is_array ($marker) ) {
			$column_slug = $marker;
			$column_name = LG_TextServices::beautify($marker);			
		} else {
			$column_slug = $marker[0];
			$column_name = $marker[1];
		}
		$ext_table = $this->ext_table;
		if ( !isset($this->ptr_extension_tables[$ext_table]))
		throw new Exception ("Trying to add a marker to DBX table '{$ext_table}' but this table is not defined!");
		if ( isset($this->ptr_columns[$column_name]) )
		throw new Exception ("Trying to set column '{$column_name}' for DBX table '{$ext_table}' but it is already defined!");
		if ( isset($this->ptr_columns[$column_name . WP_DBX_UOM_TABLE_POSTFIX]) )
		throw new Exception ("Trying to set column '{$column_name}' for DBX table '{$ext_table}' but the UOM table ({$column_name}_uom) is already define!");
		// add a convenience variable
		$column_ref = &$this->ptr_columns[$column_slug];
		// set generic attributes
		$this->base_add($column_ref,$column_slug,$column_name,$column_type,$desc,$options);
		// now do the marker specific things
		$column_ref['uom_context'] = $uom_context; // the context (things like 'distance', 'density', etc.) provides a short-list of UOM's that will be primarily determined by UOM System that the user is using.
		// now set inferred information due to being a marker
		$column_ref['insight'] = 'marker';
		$column_ref['display-type'] = 'text';
		 
		// now it's time to create the UOM column for this marker
		$this->add_marker_uom ( array($column_slug, $column_name) , $uom_context, $options );
	}
	
	public function add_marker_uom ( $marker, $uom_context, $options ) {
		$column_name;
		$column_slug;
		if ( !is_array ($marker) ) {
			$column_slug = $marker;
			$column_name = LG_TextServices::beautify($marker);			
		} else {
			$column_slug = $marker[0];
			$column_name = $marker[1];
		}
		$column_name = $column_name . " UOM";
		$column_slug = $column_slug . WP_DBX_UOM_TABLE_POSTFIX;
		$this->ptr_columns[$column_slug] = array();
		// add a convenience variable
		$column_ref = &$this->ptr_columns[$column_slug];
		// set base meta attributes
		$this->base_add($column_ref,$column_slug, $column_name . WP_DBX_UOM_TABLE_POSTFIX, WP_DBX_UOM_COL_TYPE );
		$column_ref['display-type'] = $this->get_default_display_type ( $column_name ); // TODO: come back to this and set appropriately
		// now set the marker information coming from the incoming parameters
		$column_ref['name'] = $marker . " UOM";
		// if it's a group of columns that this UOM column points then use the passed in reference to column names
		if (isset ($options['grouping_columns']) ) {
			$column_ref['desc'] = "The unit of measure for a group of columns (<small>{$options['grouping_columns']}</small>). </p><p>This UOM is constrained by the '{$uom_context}' UOM context.</p>";
		} else {
			$column_ref['desc'] = "The unit of measure for the '{$column_name}' column.This UOM is constrained by the '{$uom_context}' UOM context.";
		}
		// now set inferred info for the UOM column
		$column_ref['refers_to'] = $column_name;
		$column_ref['insight'] = 'uom';
		$column_ref['uom_context'] = $uom_context;
		$column_ref['type'] = WP_DBX_UOM_COL_TYPE; // TODO: investigate ISO standards for abbreviations and make a call on size requirements
		$column_ref['display-type'] = 'text';
	}
	
	public function add_marker_grouping ($grp_name, $column_type, $uom_context, $desc = "", $options = array() ) {
		$object = new DBX_ConfigGrouping ( $grp_name, 'marker', $this, 
						array ('column_type'=>$column_type,'uom_context'=>$uom_context,'desc'=>$desc,'options'=>$options) );
		return $object;
	}
	
	/**
	* add_marker_static
	*
	* Allows the addition of "markers" to a dbx table where the "UOM" is always the same.
	*
	* @author Ken Snyder
	*/
	public function add_marker_static ( $marker , $column_type , $static_uom , $desc = "" , $options = array() ) {
		$column_name;
		if ( !is_array ($marker) ) {
			$column_name = $marker;
			$marker = LG_TextServices::beautify($marker);			
		} else {
			$column_name = $marker[0];
			$marker = $marker[1];
		}
		$ext_table = $this->ext_table;
		if ( !isset($this->ptr_extension_tables[$ext_table]))
		throw new Exception ("Trying to add a marker to DBX table '{$ext_table}' but this table is not defined!");
		if ( isset($this->ptr_columns[$column_name]) )
		throw new Exception ("Trying to set column '{$column_name}' for DBX table '{$ext_table}' but it is already defined!");
		if ( isset($this->ptr_columns[$column_name . WP_DBX_UOM_TABLE_POSTFIX]) && $options['uom'] !== 'static')
		throw new Exception ("Trying to set column '{$column_name}' for DBX table '{$ext_table}' but the UOM table ({$column_name}_uom) is already define!");
		// add a convenience variable
		$column = &$this->ptr_columns[$column_name];
		// set generic attributes
		$this->base_add($column,$marker,$column_name,$column_type,$desc,$options);
		// now do the marker specific things
		$column['uom_context'] = 'static'; 
		$column['static_uom'] = $static_uom;
		// now set inferred information due to being a marker
		$column['insight'] = 'marker';
		$column['display-type'] = 'text';
		 
	}
	
	public function add_marker_static_grouping ($grp_name, $column_type, $uom, $desc = "", $options = array() ) {
		$object = new DBX_ConfigGrouping ( $grp_name, 'marker_static', $this, 
						array ('column_type'=>$column_type,'uom'=>$uom,'desc'=>$desc,'options'=>$options) );
		return $object;
	}
	
	
	/**
	* add_fk_reference
	*
	* Adds a column that is a foreign key to another table.
	*
	* @author Ken Snyder
	*/
	public function add_fk_reference ( $marker , $column_type , $referenced_table , $desc = "" , $options = array() ) {
		$column_name;
		global $wpdb_x;
		if ( !is_array ($marker) ) {
			$column_name = $marker;
			$marker = LG_TextServices::beautify($marker);			
		} else {
			$column_name = $marker[0];
			$marker = $marker[1];
		}
		$ext_table = $this->ext_table;
		if ( !isset($this->ptr_extension_tables[$ext_table]))
			throw new Exception ("Trying to add a marker to DBX table '{$ext_table}' but this table is not defined!");
		if ( isset($this->ptr_columns[$column_name]) )
			throw new Exception ("Trying to set column '{$column_name}' for DBX table '{$ext_table}' but it is already defined!");
		// add a convenience variable
		$column = &$this->ptr_columns[$column_name];
		// set generic attributes
		$this->base_add($column,$marker,$column_name,$column_type,$desc,$options);
		// now do the marker specific things
		$column['uom_context'] = 'static'; 
		//$column['static_uom'] = $static_uom;
		// now set inferred information due to being a marker
		$column['insight'] = 'lookup';
		$column['reference'] = $referenced_table;
		$column['display-type'] = 'text';
		$wpdb_x->add_fk($this->ext_table,$column_name,$referenced_table);
	}
	
	/**
	* add_prose
	*
	* Adds a column is meant for writing mid-to-long messages.
	*
	* @author Ken Snyder
	*/
	public function add_prose ( $marker , $desc = "" , $options = array() ) {
		$column_name;
		global $wpdb_x;
		if ( !is_array ($marker) ) {
			$column_name = $marker;
			$marker_name = LG_TextServices::beautify($marker);
		} else {
			$column_name = $marker[0];
			$marker_name = $marker[1];
		}
		$ext_table = $this->ext_table;
		if ( !isset($this->ptr_extension_tables[$ext_table]))
			throw new Exception ("Trying to add a prose column to DBX table '{$ext_table}' but this table is not defined!");
		if ( isset($this->ptr_columns[$column_name]) )
			throw new Exception ("Trying to set column '{$column_name}' for DBX table '{$ext_table}' but it is already defined!");
		// add a convenience variable
		$column = &$this->ptr_columns[$column_name];
		// set generic attributes
		$this->base_add($column,$marker_name,$column_name,'longtext',$desc,$options);
		// now set  information due to being "prose"
		$column['insight'] = 'prose';
		$column['display-type'] = 'textarea';
	}
	
	/**
	* add_json
	*
	* Adds a column is meant for putting json-encoded strings.
	*
	* @author Ken Snyder
	*/
	public function add_json ( $marker , $structure_class, $desc = "" , $options = array() ) {
		$column_name;
		if ( !is_array ($marker) ) {
			$column_name = $marker;
			$marker_name = LG_TextServices::beautify($marker);
		} else {
			$column_name = $marker[0];
			$marker_name = $marker[1];
		}
		// as this borrows many characteristics from prose set it up as prose and then override what's needed
		$this->add_prose ($marker , $desc , $options = array() );
		// add a convenience variable
		$column = &$this->ptr_columns[$column_name];
		// override properties
		$column['insight'] = 'json';
		$column['display-type'] = 'json';
		$column['structure'] = $structure_class;
	}
	
	
	/**
	* add_static_choice
	*
	* Allows the addition of a field which who's value is a constrained set of values. If this constrained should be treated only 
	* as a suggestion and allow for a user to override or add their own selections than the $options['allow_override'] should be set to true
	*
	* @author Ken Snyder
	*/
	public function add_static_choice ( $marker , $column_type , $enum , $desc = "" , $options = array() ) {
		$column_name;
		if ( !is_array ($marker) ) {
			$column_name = $marker;
			$marker = LG_TextServices::beautify($marker);			
		} else {
			$column_name = $marker[0];
			$marker = $marker[1];
		}
		$ext_table = $this->ext_table;
		if ( !isset($this->ptr_extension_tables[$ext_table]))
		throw new Exception ("Trying to add a marker to DBX table '{$ext_table}' but this table is not defined!");
		if ( isset($this->ptr_columns[$column_name]) )
		throw new Exception ("Trying to set column '{$column_name}' for DBX table '{$ext_table}' but it is already defined!");
		// add a convenience variable
		$column = &$this->ptr_columns[$column_name];
		// set generic attributes
		$this->base_add($column,$marker,$column_name,$column_type,$desc,$options);
		// now do the static_choice specific things
		if (isset($options['allow_override']) && $options['allow_override'] === true)
			$column['insight'] = 'static-choice-override';
		else
			$column['insight'] = 'static-choice';
		if ( class_exists($enum) )
			$column['enum'] = $enum;	
		else
			throw new Exception ("Unknown enumeration ({$enum}) associated to DBX column '{$column_name}' in table '{$this->ext_table}'");
		$column['display-type'] = 'select';
	}
	
	/**
	* add_datetime
	*
	* Allows the addition of a field which who's value is a constrained set of values. If this constrained should be treated only 
	* as a suggestion and allow for a user to override or add their own selections than the $options['allow_override'] should be set to true
	*
	* @author Ken Snyder
	*/
	public function add_datetime ( $marker , $desc = "" , $options = array() ) {
		$column_name;
		if ( !is_array ($marker) ) {
			$column_name = $marker;
			$marker = LG_TextServices::beautify($marker);			
		} else {
			$column_name = $marker[0];
			$marker = $marker[1];
		}
		$ext_table = $this->ext_table;
		if ( !isset($this->ptr_extension_tables[$ext_table]))
		throw new Exception ("Trying to add a marker to DBX table '{$ext_table}' but this table is not defined!");
		if ( isset($this->ptr_columns[$column_name]) )
		throw new Exception ("Trying to set column '{$column_name}' for DBX table '{$ext_table}' but it is already defined!");
		// add a convenience variable
		$column = &$this->ptr_columns[$column_name];
		// set generic attributes
		$this->base_add($column,$marker,$column_name,'datetime',$desc,$options);
		// now do the datetime specific things
		$column['insight'] = 'datetime';
		$column['display-type'] = 'date';
	}
	
	/**
	* add_binary_flag
	*
	* Allows the addition of a field which who's value is a constrained set of values. If this constrained should be treated only 
	* as a suggestion and allow for a user to override or add their own selections than the $options['allow_override'] should be set to true
	*
	* @author Ken Snyder
	*/
	public function add_binary_flag ( $marker , $desc = "" , $options = array() ) {
		$column_name;
		if ( !is_array ($marker) ) {
			$column_name = $marker;
			$marker = LG_TextServices::beautify($marker);			
		} else {
			$column_name = $marker[0];
			$marker = $marker[1];
		}
		$ext_table = $this->ext_table;
		if ( !isset($this->ptr_extension_tables[$ext_table]))
		throw new Exception ("Trying to add a marker to DBX table '{$ext_table}' but this table is not defined!");
		if ( isset($this->ptr_columns[$column_name]) )
		throw new Exception ("Trying to set column '{$column_name}' for DBX table '{$ext_table}' but it is already defined!");
		// add a convenience variable
		$column = &$this->ptr_columns[$column_name];
		// set generic attributes
		$this->base_add($column,$marker,$column_name,'boolean',$desc,$options);
		// now do the datetime specific things
		$column['insight'] = 'binary-flag';
		$column['display-type'] = 'checkbox';
	}
	
	
	/**
	* get_default_display_type
	* 
	* Helper function that "defaults" the display type if it's not been already set explicitly
	*
	* @param  hash 		$column The associative array for a given column
	* @return string 	The name of the display-type for this column
	* @author Ken Snyder
	*/
	public function get_default_display_type ( $column ) {
		// If not then switch on the database "type"		
		// list ( $db_type , $size ) =  explode ( "(" , $column['type'] , 2 );
		if ( isset ($column['display-type']) ) return $column['display-type'];
		
		if ( preg_match ( '/(.*)\((.*)\)/' , $column['type'] , $matches , PREG_OFFSET_CAPTURE ) ) {
			//error_log ( "MATCHES:" . print_r ( $matches , TRUE ) );
			$db_type =  strtolower ( $matches[1][0] );
			$size = $matches[2][0];
		}
		else $db_type = strtolower ( $column['type'] );
		$is_enumerated = isset ( $column ['enum'] );
		switch ( $db_type ) {
			case 'int':
			case 'integer':
			case 'bigint':
			return "text";
			break;
			case 'varchar':
			if ( $is_enumerated )
			return "select";
			else {
				if ( $size < 101 ) return "text";
				else return "textarea";
			}
			break;
			case 'text':
			return "text";
			break;
			case 'longtext':
			return "textarea";
			break;
			case 'datetime':
			return "date";
			break;
			default:
			return "text";
		}
	}
	
	/**
	* add_unsigned_constraint
	* 
	* adds a "unsigned" constraint to a set of columns
	*
	* @param string $ext_table The name of the extension table where the rows to be constrained are located
	* @param array $col_list The list of columns who should be set to "NOT NULL"
	* @return boolean returns true if successful, otherwise false
	* @author Ken Snyder
	*/
	public function add_unsigned_constraint ($ext_table , array $col_list ) {
		if ( !isset ( $this->ptr_extension_tables [$ext_table] ) ) {
			error_log ( 'Trying to add a UNSIGNED contraint to extension table "' . $ext_table . '" but table is not defined!' );
			return false;
		} else {
			// iterate through columns
			$error_free = true;
			foreach ($col_list as $col) {
				if ( !isset ( $this->ptr_columns[$col] ) ) {
					error_log ('Trying to add a UNSIGNED constraint to ' . $ext_table . '.' . $col . ' but this column is not defined!' );
					$error_free = false;
				} else {
					// set this into both the "columns" and "constraints" section; this is redundant but may
					// be useful structurally in some future case. If not then this can be removed from "constraints"
					$this->ptr_columns[ $col ][ 'unsigned' ] = 'unsigned';
					$this->ptr_constraints[ 'unsigned' ][ $col ] = 'true';
				}
			}
			return $error_free;
		}
	}
}


