<?php
/**
 * DBX_Resultset
 * 
 * A container of transaction detail; used to return DBX records back in a structured way
 *
 * @package database-extension-tables
 * @author Ken Snyder
**/
class DBX_Resultset {
	// Constants
	// -----------------------------------
	
	
	// Public Properties
	// -----------------------------------
	public $base_data = array();
	public $activity_data = array();
	public $dbx_data = array();

	// Protected/Private instance variable
	// -----------------------------------


	// Constructor
	// -------------------------------
	public function __constructor () {
		
	}
	
	// Public API
	// -------------------------------
	
	/**
	* get_results
	* 
	* returns all data as an associative array collapsing the distinction between base, activity, and dbx data.
	*
	* @return type						description
	**/
	public function all_data () {
		return array_merge($this->base_data,$this->activity_data,$this->dbx_data);
	}
	


	// Protected API
	// -------------------------------
	
}


