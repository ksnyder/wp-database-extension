<?php

/**
 * DBX_UtilityFunctions class
 *
 * A library of utility functions 
 *
 * @package database-extension-tables
 * @author Ken Snyder
 **/
class DBX_AbstractionMap 
{
	// Const
	
	// Instance Variables
	public $name;
	public $default_values;
	public $post_type_overrides;
	
	function __construct ( $name , $defaults = false) {
		$this->name = $name;
		if ( $defaults ) $this->set_defaults ( $defaults );
	}
	
	public function set_defaults ( $associative_array ) {
		foreach ( $associative_array as $attr ) {
			$this->set_default_value ( $attr['variable'] , $attr['table'] , $attr['column'] );
		}
	}

	public function set_default_value ( $variable , $table_abbeviation , $column ) {
		$this->default_values [$variable] = array ( 'table' => $table_abbeviation , 'column' => $column );
	}
	
	public function add_override ( $post_type , $variable , $table_abbeviation , $column ) {
		if ( !isset ($this->post_type_overrides [$post_type]) ) $this->post_type_overrides [$post_type] = array();
		$this->post_type_overrides [$post_type][$variable] = array ( 'table' => $table_abbeviation , 'column' => $column );
	}
	
	public function get_map_for_post_type ( $post_type ) {
		return array_merge ( $this->default_values , $this->post_type_overrides[$post_type] );
	}
	
	public function has_override ( $post_type ) {
		if ( !isset ( $this->post_type_overrides [$post_type] ) ) return true;
		else return false;
	}	


} // END class 


	
	
	
/* --------- END OF FILE ---------- */