<?php
/**
* @package		database-extension-tables	
* @copyright 	LifeGadget Limited, 2012	
*/

// GLOBAL CONSTANT DEFINITIONS


/**
* A container of meta-data about the constraint of a particular DBX column.
*
* @package		database-extension-tables	
* @author 		Ken Snyder
*/
class DBX_FieldConstraint {

	/**
	 * the type of constraint this column is under. Valid values include "none", "enumeration", "numeric"
	 *
	 * @var string
	*/
	public $type;
	/**
	* The enumeration that constrains this DBX column. This will only be set if the $type is set to "enumeration". In that case, the value will 
	* represent the name of the enumeration that constrains this field. By default this value is set to false.
	*
	* @var string|false */
	public $enumeration = false; 
	/**
	* If the DBX column is an enumeration, the typical constraint would be a "hard-constraint" which means the only valid values are those defined in
	* the enumeration, however, it is possible that there will be fields -- set with an insight of "static-choice-override" that are guided by the 
	* enumeration but allow the user to create items not on the enumerated list.
	*
	* @var boolean */
	public $soft_constraint = null; 
	/**
	* The minimum numeric value this field is allowed to hold. If there are no constraints on the minimum value than it will default to a value of false.
	*
	* @var float|int|false */
	public $numeric_min = false; 
	/**
	* The maximum numeric value this field is allowed to hold. If there are no constraints on the maximum value than it will default to a value of false.
	*
	* @var float|int|false */
	public $numeric_max = false; 
	/**
	* Fields that need to conform to a particular string structure will have a regular expression set here otherwise this will remain in a false state.
	*
	* @var string|false */
	public $structure = false; 

	/**
	* Constructor
	* 
	* the public constructor for DBX_FieldConstraint class definition */
	public function __constructor () {
		
	}
}


// END OF FILE


