<?php
/**
* In cases where there are several fields/columns in a DBX table that are related contextually and measured with the same unit-of-measure,  
* this class offers a convenient way to configure this grouping. This is not the same as "FieldGrouping" which
* provides a way to create a contextual set but doesn't imply anything about UOM similarity. 
* 
* @package database-extension-tables
**/
class DBX_ConfigGrouping {
	
	// Instance Variables
	protected $ptr_configurator;
	protected $group_name;
	protected $group_type;
	protected $parameters;
	

	public function __construct ( $group_name, $group_type, &$ptr_configurator , array $parameters ) {
		$this->ptr_configurator = $ptr_configurator;
		$this->group_name = $group_name;
		$this->group_type = $group_type;
		$this->parameters = $parameters;
	}
	
	public function add_items( array $items ) {
		$marker="";
		$column_name="";
		$item_list="";
		
		foreach ($items as $item) {
			// we need to add the group's name as a prefix for the column name
			if (is_array($item)){
				list($column_name,$marker) = $item;
				$column_name = $this->group_name . "_" . $column_name;
				$item = array ($column_name,$marker);
			} else {
				$item = $this->group_name . $item;
			}
			// add this item to the list
			if (empty($item_list) )
				$item_list = $column_name;
			else
				$item_list = $item_list . ", " . $column_name;
			// now execute based on the TYPE of marker
			switch ($this->group_type) {
				case "marker":
					// add all items as a "static marker" and then (outside the foreach loop) add a UOM field for the grouping
					$this->parameters['uom'] = 'GROUP_UOM';
				case "marker_static":
					// this step is consistent for both markers and static markers
					$this->ptr_configurator->add_marker_static ( $item,
										$this->parameters['column_type'],
										$this->parameters['uom'],
										$this->parameters['desc'],
										$this->parameters['options']
					);
					break;
			}	
			
		} // end foreach
		
		// if this is a grouping of markers, add a field for UOM		
		if ($this->group_type === "marker") {
			$uom_column_name = $this->group_name .  "_grp";
			$uom_marker = LG_TextServices::beautify ($this->group_name);
			$options['grouping_columns'] =  $item_list;
			$this->ptr_configurator->add_marker_uom (array ($uom_column_name, $uom_marker), $this->parameters['uom_context'], $options );
		}
	}

}