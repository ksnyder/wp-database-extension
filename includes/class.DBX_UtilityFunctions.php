<?php
/**
 * DBX_UtilityFunctions class
 *
 * A library of utility functions 
 *
 * @package database-extension-tables
 * @author Ken Snyder
 **/
class DBX_UtilityFunctions 
{
	// Const
	
	// Instance Variables

	/**
	* beautify
	* 
	* Take a "slug-like" name and transform it into a nicer language like name.
	*
	* @param 	string 	$ugly_name 	the slug-like name
	* @return 	string 				the pretty name
	* @author Ken Snyder
	*/
	public static function beautify ($ugly_name ) {
		return ucwords( str_replace( '_', ' ', $ugly_name ) );
	}
	
	public static function seperate_type_and_size ( $mixed_variable ) {
		if ( preg_match ( '/(.*)\((.*)\)/' , $mixed_variable , $matches , PREG_OFFSET_CAPTURE ) ) {
			$type =  trim(strtolower ( $matches[1][0] ) );
			$size = $matches[2][0];
			return array ( $type , $size );
		}
		else {
			$type = strtolower ( $mixed_variable );
			return array ( $type , false );
		}
		
	}

} // END class 


	
	
	
/* --------- END OF FILE ---------- */