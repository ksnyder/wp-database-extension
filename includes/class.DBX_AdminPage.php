<?php
/**
 * DBX_AdminPageHelper class
 *
 * library of functions that build the administrative screens of this plugin
 *
 * @package database-extension-tables
 * @author Ken Snyder
 **/
class DBX_AdminPage 
{
	// Const
	
	// Instance Variables

	// Static Initializer
	static public function init () {
		// Add wp-db-ext to the Settings menu
	 	// add_management_page ( __( 'DBX Extension', 'dbx_menu' ), __( 'WP DB Extension', 'dbx_menu' ), 'manage_options', 'dbx_menu',  'DBX_AdminPage::show_admin_page'  );
		add_submenu_page ( LG_ADMIN_MENU, 'Database Extension Tables' , 'DBX Admin', 'manage_options', 'dbx-admin', 'DBX_AdminPage::show_admin_page'  );
	}
	
	/**
	* print_admin_page
	* 
	* Prints the overall admin page which found under settings->WP DB Extensions
	*
	* @param void  
	* @return void 
	* @author Ken Snyder
	*/
	static public function NEW_show_admin_page() {
		global $wpdb_x;
		$page = FE_Template::init();
		$page->assign ( 'wpdb_x' , $wpdb_x);
		$page->display( "admin_pages/dbx_ext_tables.tmpl");
	}
	
	
	static public function show_admin_page () {
		// load the plugin settings
		global $wpdb_x;
		$page = FE_Template::init();
		
		
		// note the bootstrap issue here: https://github.com/twitter/bootstrap/issues/2538
		?>
		<div class="wrap">
			<div id="icon-options-general" class="icon32">
				<br>
			</div>
			<h2><?php _e( 'WordPress - Database Extension Tables (DBX)', DBX_LOCALISATION ); ?></h2>
			<p>
				<?php _e( 'This admin panel is primarily read-only and is only meant to give an admin meta information about the current configuration. Changes to this configuration are in code and should be changed in code.', DBX_LOCALISATION ); ?>
			</p>
			<?php 
			if (!empty($_POST["add_ext_table"]) ) {
				// TODO: implement adding a new table
				try {
					$result = $wpdb_x->create_table ( $_POST['add_ext_table'] );
				} catch (Exception $e) {
					?>
					<div class="alert alert-block alert-error">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<h4 class="alert-heading">Error adding Extension Table (<?php echo $_POST['add_ext_table']; ?>)</h4>
						<p><?php echo $e->getMessage(); ?></p>
					</div>
					<?php
				}
				if ( isset ($result) && $result ) {
					?>
					<div class="alert alert-block alert-success">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<h4 class="alert-heading">Extension Table Added</h4>
						<p>The extension table "<?php echo $_POST['add_ext_table']; ?>" was added to the database.</p>
					</div>
					<?php
				}
			}
			
			if (!empty($_POST["add_column_to_ext_table_confirmed"]) ) {
				// add a new column to an existing table
				$table = esc_html ( $_POST['dbx_table'] );
				$column = esc_html ( $_POST['dbx_column'] );
				try {
					$result = $wpdb_x->add_column_to_database ( $table , $column );
				} catch (Exception $e) {
					?>
					<div class="alert alert-block alert-error">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<h4 class="alert-heading">Error adding column to extension table</h4>
						<p>When trying to add <i><?php echo $column; ?></i> to the <b><?php echo $table; ?></b> extension table the following error was encountered:</p>
						<p><?php echo $e->getMessage(); ?></p>
					</div>
					<?php
				}
				if ( isset ($result) && $result ) {
					?>
					<div class="alert alert-block alert-success">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<h4 class="alert-heading">Column added to Extension Table</h4>
						<p>The extension table "<?php echo $_POST['dbx_column']; ?>" was added to the <b><?php echo $_POST['dbx_table']; ?></b> extension table.</p>
					</div>
					<?php
				}
			}
			
			
			if (!empty($_GET["add_column_to_ext_table"])) {
				$table = esc_html ( $_GET['dbx_table'] );
				$column = esc_html ( $_GET['dbx_column'] );
				?>
				<div class="alert ">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<h4 class="alert-heading">Add column to "<?php echo $table; ?>" Extension Table</h4>
					<p>You have indicated you wanted to add the "<?php echo $column; ?>" column to the <?php echo $table; ?> extension table. You can add columns to a table non-destructively but be warned that if the <?php echo $table; ?> table has a large number of records this can take a long time (the process includes doing a full copy of the data before adding the new column). If this is in a test instance (where data is invariably light) or you know the data is a controlled amount then this will be a non-issue. </p>
					<p>
						<form method="post" action="admin.php?page=dbx-admin">
							<input type="hidden" name="dbx_table" value="<?php echo $table; ?>">
							<input type="hidden" name="dbx_column" value="<?php echo $column; ?>">
							<button class="btn btn-warning submit" name="add_column_to_ext_table_confirmed" value="true">Add "<?php echo $column; ?>" to <b><?php echo $table; ?></b> extension table</button>
						</form>
					</p>
				</div>
				<?php
			}
			
			?>
			<form method="post" action="admin.php?page=dbx-admin">
				<div id="poststuff" class="metabox-holder dbx">
					<div class="tabbable">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#ext_tbl" data-toggle="tab">Extension Tables</a></li>
							<li><a href="#pt_rel" data-toggle="tab">Post-Type Relationships</a></li>
							<li><a href="#about" data-toggle="tab">About</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active " id="ext_tbl">
								<!-- EXTENSION TABLES -->
								<div class='tabbable tabs-left' >
									<ul class="nav nav-tabs">
										<?php $ext_tables = $wpdb_x->get_extension_tables();
										$first_item = true;
										foreach ($ext_tables as $ext_table) {
											if ($first_item) {
												$active_class = "class='active'";
												$first_item = false;
											} else {
												$active_class = "";
											}
											if ( DBX_DataAccess::db_table_exists ( $ext_table ) ) {
												$table_warning = "";
											} else {
												$table_warning = "<i class='icon-exclamation-sign' data-original-title='table does not exist in database' rel='tooltip'></i>";
											}
											echo "<li $active_class><a href='#$ext_table' data-toggle='tab'>$ext_table $table_warning</a></li>";
										}								
										?>
									</ul>
									<div class="tab-content">
										<?php
										$first_item = true;
										foreach ($ext_tables as $ext_table) { 
											$db_definition = DBX_DataAccess::db_table_definition ( $ext_table );
											if ($first_item) {
												$active_class = "active";
												$first_item = false;
											} else {
												$active_class = "fade";
											}
											echo "<div class='tab-pane $active_class' id='$ext_table'><p>"; 
												?>
											<h1>
												Data Properties <i>for</i> <?php echo "$ext_table</h1>"; 
												if ( !DBX_DataAccess::db_table_exists ( $ext_table ) )  {  ?>
													<div class="alert alert-block alert-error">
														<button type="button" class="close" data-dismiss="alert">×</button>
														<h4 class="alert-heading">Extension Table Missing!</h4>
														<p>The extension table "<?php echo $ext_table; ?>" is not setup in the database. You can add it manually to the database, add a "$wpdb_x->create_table( $ext_table )' call in your code, or just use the button below to add it. The full name of the table in your database should be called: "<?php echo $wpdb_x->get_ext_table_full_name( $ext_table);  ?>".</p>
														<p>
															<button class="btn btn-danger submit" name="add_ext_table" value="<?php echo $ext_table; ?>">Add table to database</button>
														</p>
													</div>
													<?php
												} ?>												

											<h2>Table Structure</h2>
											<table class="table table-striped">
												<thead>
													<tr>
														<th width="90px">Column</th>
														<th width="65px">Type</th>
														<th width="65px">Constraints</th>
														<th>Description</th>
														<th width="30px">Status</th>
													</tr>
												</thead>
												<tbody>
											<?php
											foreach ( $wpdb_x->get_columns_meta ( $ext_table ) as $column => $attr ) {
												?>
												<tr>
													<td><?php 	echo $column;
																if ( $wpdb_x->is_pk ($ext_table , $column)){ ?>&nbsp;<span class="label label-inverse small pointer">pk</span><?php }
																else if ( $wpdb_x->is_fk ( $ext_table , $column )) {
																?>&nbsp;<span class="label small pointer"><span rel="tooltip" class="pointer" data-original-title="foreign key: <?php echo $attr['FK'] ?>">fk</span></span><?php } ?>
													</td>
													<td><?php echo $attr['type']?></td>
													<td><?php
													if ( isset( $attr['unsigned'] ) ) echo "<span class='label small pointer'><span rel='tooltip' data-original-title='unsigned'>+/-</span></span>&nbsp;";
													if ( isset( $attr['enum'] ) ) {
														$enum = new $attr['enum'];
														$enum_list = array();
														foreach ($enum->list_keys() as $key) {
															$key = esc_html ( $key );
															if ( $key === $enum->{$key} ) array_push ( $enum_list , $key );
															else array_push ( $enum_list , "$key => " . $enum->{$key} );
														}
														$enum_string = implode ( ',</br>' , $enum_list );
														
														echo "<span class='label label-success small pointer'><span rel='tooltip' data-original-title='<b>{$attr['enum']}:</b><br/> $enum_string'>{e}</span></span>&nbsp;";
													}
													if ( isset( $attr['nullable'] ) ) echo "<span class='label label-warning small pointer'><span rel='tooltip' data-original-title='not nullable'>&nbsp;⊘&nbsp;</span></span>&nbsp;";
													?></td>
													<td><?php echo $attr['desc']
													?></td>
													<td class="small">
														<?php
															// Check for inconsistencies between code and db models
															$column_is_ok = true;
															if ( !DBX_DataAccess::db_table_exists ( $ext_table ) ) {
																self::status_label ( '' , 'n/a', 'The column definition does not exist because the database table is NOT defined!');
																$column_is_ok = false;
															} else if ( ! isset ( $db_definition [$column] ) ) {
																echo "<a href='admin.php?page=dbx-admin&add_column_to_ext_table=true&dbx_column={$column}&dbx_table={$ext_table}'>";
																self::status_label ( 'label-important' , 'DNE', 'This column does not exist in the database! <p>Click to add this to the database.</p>');
																echo "</a>";
																$column_is_ok = false;
															} else {
																// we know that the column DOES exist, now checking that it's properties are the same
																$db_def = $db_definition [$column];
																$cd_def = $wpdb_x->get_columns_meta ( $ext_table , $column );
																list ( $db_type , $db_size ) = DBX_UtilityFunctions::seperate_type_and_size ( $db_def->COLUMN_TYPE );
																list ( $cd_type , $cd_size ) = DBX_UtilityFunctions::seperate_type_and_size ( $cd_def['type'] );
																// $cd_def = $wpdb_x->get_columns_meta ( $ext_table , $column );
																if ( $db_type !==  $cd_type ) {
																	$alias_type = DBX_DataAccess::database_type_alias ( $cd_type );
																	if ( $db_type !== $alias_type )
																		self::status_label ( 'label-important' , 'type' , "The data type is different. The database has '{$db_type}' but in code it is '{$cd_type}'.");
																	else 
																		self::status_label ( '' , 'alias' , "The data type {$cd_type} is aliased to {$alias_type}; so while the database and code don't explicitly match, with the alias considered it is a match." );
																	echo "&nbsp;";
																	$column_is_ok = false;
																}
																if ( $db_size !==  $cd_size && $db_size !== DBX_DataAccess::default_size_by_type ( $cd_type ) ) {
																	if ( DBX_DataAccess::default_size_by_type ( $db_type ) )
																		self::status_label ( '' , 'size' , "The size of the data type is different but this is an integer type so the 'size' is really just a suggestion to the frontend on presentation rather than precision specification. This is probably not a real issue but the database has '{$db_size}' while in code it is '{$cd_size}'. Typically for integer types of {$db_type} the default value is " . DBX_DataAccess::default_size_by_type ( $cd_type ) );
																	else 
																		self::status_label ( 'label-important' , 'size' , "The size of the data type is different. The database has '{$db_size}' but in code it is '{$cd_size}'.");
																	$column_is_ok = false;
																}
																$col_ddl = $wpdb_x->get_column_ddl ( $ext_table , $column );
																if (( $db_def->IS_NULLABLE === "YES" && !$col_ddl->nullable ) || ( $db_def->IS_NULLABLE === "NO" && $col_ddl->nullable ) ) {
																	$message_tail = ( $col_ddl->nullable ) ? "it is NOT nullable" : "it is nullable";
																	self::status_label ('label-important' , 'null' , "The database and code disagree on whether this attribute is 'nullable'. The database states that {$message_tail} and the code states the opposite.");
																	$column_is_ok = false;
																}
															}
															
															if ( $column_is_ok ) 
																self::status_label ( 'label-success' , 'ok', 'The database and code are in sync');
														?>
													</td>
										<?php } ?>
										</tr>
									</tbody>
								</table>
								<p></p>
								<h2>JSON Dictionary</h2>
								<?php
								$attributes_meta = $wpdb_x->get_attributes_meta ( $ext_table );
								if ( count ( $attributes_meta ) === 0 ) {
									echo "There are no defined dictionary items for '$ext_table' extension table.";
								} else { ?>
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="90px">Property</th>
												<th width="65px">Struct</th>
												<th>Attributes</th>
												<th>Description</th>
											</tr>
										</thead>
										<tbody> <?php
										foreach ( $wpdb_x->get_attributes_meta ( $ext_table ) as $property => $attr ) {  ?>
											<tr>
												<td><?php echo $property; ?></td>
												<td>
													<?php 
												switch ( $attr['type'] ) {
													case ('OBJECT'):
														echo "<span class='tooltip-top' rel='tooltip' data-original-title='class definition'>{$attr['class']}</span>";
														break;
													case ('ARRAY_A'):
														echo "<span class='tooltip-top' rel='tooltip' data-original-title='class definition'><b>array(</b>\${$attr['key']} => {$attr['class']}</span><b>)</b>";
														break;
													case ('ARRAY_N'):
														echo "<span class='tooltip-top' rel='tooltip' data-original-title='class definition'><b>array(</b>{$attr['class']}</span><b>)</b>";
														break;
													default:
														echo "<span class='tooltip-top' rel='tooltip' data-original-title='php native type'>{$attr['type']}</span>";
												}
													?>
												</td>
												<td>
													<?php
													if ( isset($attr['attributes']) ) {
														foreach ( $attr['attributes'] as $attr_name => $attr_value ) {
															echo "<span class='dictionary attribute label' rel='tooltip' data-original-title='{$attr_value}'>";
															echo "{$attr_name}</span> ";
														}
													}
													
													if ( preg_match ( "/ARRAY/" , $attr['type'] ) > 0 ) {
														if ( !isset ( $attr['class'] ) ) {
															self::status_label ( 'label-warning' , 'undefined' , "the configuration didn't specify a class definition for this object. Add a 'class => [class]' statement in the configuration file." );
														} else {
															if ( !class_exists ( $attr['class'] ) ) {
																self::status_label ( 'label-warning' , $attr['class'] , 'the class definition could not be found' );
															} else {
																$object = new $attr['class'];
																$class_properties = $object->describe();
																
																foreach ( $class_properties->property->public as $prop => $desc ) {
																	self::status_label ( 'label-inverse' , $prop , $desc , 'top' );
																	echo " ";
																}
																foreach ( $class_properties->property->accessor as $prop => $desc ) {
																	self::status_label ( '' , $prop , $desc , 'top' );
																	echo " ";
																}
																foreach ( $class_properties->function->public->getter as $prop => $desc ) {
																	self::status_label ( 'label-success' , $prop . '()' , $desc , 'top' );
																	echo " ";
																}
																foreach ( $class_properties->function->public->setter as $prop => $desc ) {
																	self::status_label ( 'label-warning' , $prop . '()' , $desc , 'top' );
																	echo " ";
																}
																
															}
														}
													} 
													?>
												</td>
												<td><?php echo $attr['desc']?></td>
											</tr>
										<?php } // and FOREACH loop for json attributes
										echo "</table>"; 
								} // end IF condition  ?>
								<p></p>
								<h2>Field Groupings</h2>
								<?php
								$field_groupings = $wpdb_x->get_groupings ( $ext_table );
								if ( count ( $field_groupings ) === 0 ) {
									echo "There are no field groupings for the $ext_table extension table";
								} else {  ?>
									<table class="table table-striped">
										<thead>
											<tr>
												<th>Field Grouping</th>
												<th>Display Name</th>
												<th>Fields</th>
												<th>Filters</th>
											</tr>
										</thead>
										<tbody>
										<?php
										foreach ( $field_groupings as $group ) { 
											echo "<tr>";
											echo "<td>" . $group->get_slug() . "</td>";
											echo "<td>" . $group->get_name() . "</td>";
											// Display FIELDS column
											echo "<td>";
											$i = 1;
											foreach ( $group->get_fields() as $field ) {
												if ( $i % 3 ) $seperator = ", ";
												else $seperator = ",<br/>";
												echo "$field$seperator";
												$i++;
											}
											echo "</td>";
											// Display FILTERS column
											echo "<td><table class='table-condensed'>";
											foreach ( $group->get_filters() as $filter => $filter_fields ) {
												$filter_fields = implode ( ',<br/>' , $filter_fields );
												echo "<tr><td><span rel='tooltip' data-original-title='$filter_fields'>$filter</span></td></tr>";
											}
											echo "</table></tr>";
										} // end FOREACH 
										?>
									</tbody>
								</table>
								<?php } // end IF-ELSE ?>
							</div>
							<?php } // end foreach for ext tables ?>		
						</div> <!-- END OF EXT TABLE CONTENT AREA  -->
					</div> <!-- END of "Tabbable" for Tables -->
				</div> <!-- END of EXT TABLE area -->
				<div class="tab-pane fade" id="pt_rel">
					<h1>Post-Type Relationships</h1>
					<h2>Post-type to Extension Table(s)</h2>

					<table class="table ">
						<thead>
							<tr>
								<th width="90px">Post-Type</th>
								<th width="65px">Extension Table(s)</th>
								<th width="65px">Abstraction Map(s)</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($wpdb_x->get_mapping_relationships() as $cpt => $ext_tables) { ?>
								<tr>
									<td><?php echo $cpt; ?></td>
									<td>
										<table class='table-bordered table-condensed'>
											<?php 	
											foreach ( $ext_tables as $ext_table ) {
													echo "<tr><td>$ext_table</td></tr>";
												} ?>
										</table>
									<td>tbd</td>
									<?php
							} // end foreach for ?>
							</tbody>
						</table>	
					</div>	
					<div class="tab-pane fade" id="about">
						<p>You are using version <?php echo WP_DATABASE_EXTENSION ?>  of the Database Extension (DBX) plugin.</p>
						<p>This plugin was authored by <a href="http://ken.net">Ken Snyder</a> and is offered under the GNU public license for you to use. If you do end up using this plugin and making changes please let me know and I will try to incorporate your changes back into the GIT responsitory (<a href="https://github.com/ksnyde/wp-database-extension">GitHub Repo</a>).</p>
					</div>
				</div> <!-- end tab content -->
			</div>		
		</div>
		<?php
		// Display the submit button
		// submit_button();?>
	</form>
</div>
<?php
}

	/**
	* register_meta_boxes
	* 
	* Add the appropriate meta boxes for the admin page and point to callback functions 
	* to have the boxes populated
	*
	* @param   
	* @return none 
	* @author Ken Snyder
	*/
	static public function register_meta_boxes () {
		add_meta_box ( 'wp-db-ext-tables' , __( 'Extension Tables' , DBX_LOCALISATION ), array ( &$this , 'print_extension_table' ) , 'post' );
		add_meta_box ( 'wp-db-ext-post-2-table' , __( 'Post Type to Table Relationships' , DBX_LOCALISATION ), array ( &$this , 'print_post_2_table' ) , 'post' );
	}
	
	/**
	* print_extension_table
	* 
	* prints out the table of defined extension tables and indicates:
	* a) whether the DB and code are in sync (if not in DB then a "create" button will automate creation)
	* b) how many rows are in the existing extension table
	* c) date that table was created
	* d) a popover that describes the table structure of the table 
	*
	* @return void
	* @author Ken Snyder
	*/
	static public function print_extension_table () {
		// TODO: build this
		?>
		<div class="wrap">
			needs to be implemented
		</div>
		<?php 
	}
	
	/**
	* print_post_2_table
	* 
	* prints out the post-types-to-extension table assignments
	*
	* @return void
	* @author Ken Snyder
	*/
	static public function print_post_2_table () {
		// TODO: build this
		?>
		<div class="wrap">
			needs to be implemented
		</div>
		<?php 
	}
	
	static public function status_label ( $class_addins , $label_text, $message , $location = 'left') {
		echo "<span class='label $class_addins'><span class='tooltip-{$location}' rel='tooltip' data-original-title=" . '"' . $message . '">' . $label_text . '</span></span>';
	}

} // END class 
	
/* END OF FILE */
