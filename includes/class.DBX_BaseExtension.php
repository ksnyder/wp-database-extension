<?php

/**
 * A base class that allows for an easy mechanism to gain access to the DBX state machine
 *
 * @package database-extension-tables
 * @author Ken Snyder
 */
abstract class DBX_BaseExtension {
	// Instance variables
	// ------------------
	/** a pointer to the primary DBX state mechanism for column and table information **/
	protected $ptr_extension_tables;
	protected $ptr_grouping_reference;
	
	public function __construct () {
		global $wpdb_x;
		$this->ptr_extension_tables =& $wpdb_x->get_table_reference();
		$this->ptr_grouping_reference =& $wpdb_x->get_grouping_reference();
		$this->ptr_mapping_tables =& $wpdb_x->get_mapping_tables();
	}
	
	public function &get_col_ref ($ext_table) {
		return $this->ptr_extension_tables[$ext_table]['columns'];
	}

	public function &get_constraints_ref ($ext_table) {
		return $this->ptr_extension_tables[$ext_table]['constraints'];
	}
	
}