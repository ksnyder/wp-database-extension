<?php
/**
* a set of functions that provide meta-information about extension tables (but not the transactional data within those tables).
* 
* @package database-extension-tables
**/
class DBX_Meta extends DBX_BaseExtension {

	/**
	 * Constructor
	 *
	 * Constructor for DBX_Meta class
	 */
	public function __construct () {
		parent::__construct();
	}
	
	/**
	 * Lists the extension tables (note: this proxies the functionality in DBX_TableExtension). This can be scoped in three ways: 
	 * a) if called with no parameters than ALL extension tables are returned 
	 * b) if a post_type is passed in than it will be constrained to only those associated to that post_type 
	 * c) if an attribute is passed in than it will only return extension tables that have this ID
	 *
	 * @param string $post_type String name/slug for post-type (optional param)
	 * @param string $attribute String name/slug for post-type (optional param)
	 * @see DBX_TableExtension->get_extension_tables
	 * @return void
	 * @author Ken Snyder
	 */
	public function get_extension_tables($post_type=false,$attribute=false) {
		global $wpdb_x;
		return $wpdb_x->get_extension_tables($post_type,$attribute);
	}
	
	public function full_table_name($ext_table) {
		global $wpdb_x;
		return $wpdb_x->get_ext_table_full_name($ext_table);
	}

	/**
	 * returns all of the column meta information for a given dbx table
	 *
	 * @param		string		$table 
	 * @return		array
	*/
	public function get_col_meta($table) {
		return $this->get_col_ref($table);
	}
		
	/**
	 * dbx_field_exists
	 *
	 * tests whether a particular field exists in a CPT or DBX table
	 *
	 * @param string $scope 	The "scope" is either a custom-post-type (checked first) or a DBX table (short name, not DB name)
	 * @param string $field 	The field/column being tested for
	 * @return boolean			
	 * @author Ken Snyder
	 */
	public function dbx_field_exists($scope,$field) {
		// first test to see if $scope is a "custom-post-type"
		global $CustomTypes, $wpdb_x;
		if ( in_array($scope,$CustomTypes->get_post_types()) ) {
			// it's a CPT; first thing to do is resolve to a set of DBX tables
			$dbx_tables = $this->get_extension_tables ($scope);
			foreach ($dbx_tables as $ext_table) {
				//$column = $this->get_col_ref($ext_table);
				$column = $wpdb_x->get_columns($ext_table);
				if ($wpdb_x->column_exists($ext_table,$field))
					return true;
			}
			return false;
		} else {
			// ok, it's just a DBX table being passed in; very easy then
			$column = $this->get_col_ref($table);
			return isset($column[$field]);
		}
	}
	
	/**
	* field_insight
	* 
	* Passes back the 'insight' meta characteristic of a DBX column.
	*
	* @param string  	$table		the DBX table (shortcut name not fully qualified)
	* @param type 		$field		the DBX column
	* @return string				the type of 'insight' that this field has
	*/
	public function field_insight($table, $field) {
		$column = $this->get_col_ref($table);
		if (isset($column[$field]['insight']))
			return $column[$field]['insight'];
		else
			return "";
	}
	
	public function field_of_type($table,$type) {
		$columns = $this->get_col_ref($table);
		$list = array();
		foreach($columns as $col=>$attr) {
			if (isset($attr['insight']) && $attr['insight']===$type) 
				array_push($list,$col);
		}
		return $list;
	}
	
	public function field_list($table) {
		$columns = $this->get_col_ref($table);
		$list = array();
		if ( !empty($columns) ) {
			foreach($columns as $col=>$attr) {
				array_push($list,$col);
			}
		}
		
		return $list;
	}
	
	/**
	 * field_name
	 *
	 * returns the human-friendly name of the DBX column
	 *
	 * @param		string		$table The DBX extension table 
	 * @param		string		$field The column/field name
	 * @return		void
	 * @author		Ken Snyder
	*/
	public function field_name($table,$field) {
		$column = $this->get_col_ref($table);
		return $column[$field]['name'];
	}
	/**
	 * field_desc
	 *
	 * returns a description of the DBX column
	 *
	 * @param		string		$table The DBX extension table 
	 * @param		string		$field The column/field name
	 * @return		void
	 * @author		Ken Snyder
	*/
	public function field_desc($table,$field) {
		$column = $this->get_col_ref($table);
		if(isset($column[$field]['desc']))
			return $column[$field]['desc'];
		else
			return "";
	}
	/**
	 * physical_type
	 *
	 * returns the physical database type (varchar, integer, etc.)
	 *
	 * @param		string		$table The DBX extension table 
	 * @param		string		$field The column/field name
	 * @return		void
	 * @author		Ken Snyder
	*/
	public function physical_type($table,$field) {
		$column =& $this->get_col_ref($table);
		if (isset($column[$field]['type']))
			return $column[$field]['type'];
		else
			return "undefined";
	}
	
	/**
	 * get_field_meta
	 *
	 * returns an associative array of meta attributes of a particular table/field in DBX.
	 *
	 * @param		string		$table 
	 * @param		string		$field 
	 * @return		void
	 * @author		Ken Snyder
	*/
	public function get_field_meta($table,$field) {
		$column = $this->get_col_ref($table);
		if (isset($column[$field])) {
			return $column[$field];
		} else {
			return array();
		}
	}
	
	/**
	 * set_field_meta
	 *
	 * allows a 
	 *
	 * @param		string		$table 
	 * @param		string		$field 
	 * @param		string		$dbx_column 
	 * @return		void
	 * @author		Ken Snyder
	*/
	public function set_field_meta($table,$field,$dbx_column) {
		$column =& $this->get_col_ref($table);
		if (isset($column[$field])) {
			\AppLogger::debug ( "SETTING {$field} to \n" . print_r ( $dbx_column , TRUE ) ); 
			$column[$field] = $dbx_column;
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * rename_column
	 *
	 * rename a dbx column in the configuration and in the database
	 *
	 * @param		string		$table 
	 * @param		string		$original_name 
	 * @param		string		$changeto_name 
	 * @return		void
	 * @author		Ken Snyder
	*/
	public function rename_column($table,$original_name,$changeto_name) {
		$columns =& $this->get_col_ref($table);
		// change the column name in the database
		\DBX_DataAccess::rename_column($table,$original_name,$changeto_name);		
		// change the configuration
		$columns = LG_Utility::replace_array_key($columns,$original_name,$changeto_name);
	}
	
	public function add_column($table,$column_name=false) {
		$columns_ref =& $this->get_col_ref($table);
		if (!$column_name) {
			$column_name = "COLUMN_" . \DBX_DataAccess::column_count($table);
		}
		// add to the configuration
		if (!isset($columns_ref[$column_name])) {
			$columns_ref[$column_name] = array();
			$column = new LG\Structure\DbxColumn($table,$column_name);
			$column->insight = "string-value";
			$column->type = "varchar(100)";
			$column->name = $column->id;
			$column->desc = "(description)";
			$column->save();
			
			// now add to DB
			\DBX_DataAccess::add_column($table,$column_name,$column->type);
		}
	}

	
	/**
	 * is_unsigned
	 *
	 * boolean test for whether a field is unsigned
	 *
	 * @param		string		$table The DBX extension table 
	 * @param		string		$field The column/field name
	 * @return		boolean
	 * @author		Ken Snyder
	*/
	public function is_unsigned($table,$field) {
		$contraints = $this->get_constraints_ref($table);
		return isset($contraints['unsigned'][$field]);
	}
	/**
	 * is_not_null
	 *
	 * boolean test for whether a field is "not null"
	 *
	 * @param		string		$table The DBX extension table 
	 * @param		string		$field The column/field name
	 * @return		boolean
	 * @author		Ken Snyder
	*/
	public function is_not_null($table,$field) {
		$contraints = $this->get_constraints_ref($table);
		return isset($contraints['NOT_NULL'][$field]);
	}
	

	
	/**
	 * field_enum
	 *
	 * returns the name of the enumeration class
	 *
	 * @param		string		$table The DBX extension table 
	 * @param		string		$field The column/field name
	 * @return		string 		the class name of the enumeration class that constrains this field; false if not found
	 * @author		Ken Snyder
	*/
	public function field_enum($table,$field) {
		$columns = $this->get_col_ref($table);
		if (isset($columns[$field]['enum']) && $columns[$field]['enum'] !== "")
			return $columns[$field]['enum'];
		else
			return false;
	}
	
	/**
	 * field_enum_tooltip
	 *
	 * create a tooltip HTML for an enumeration
	 *
	 * @param		string		$table 
	 * @param		string		$field 
	 * @return		void
	 * @author		Ken Snyder
	*/
	public function field_enum_tooltip($table,$field) {
		$columns = $this->get_col_ref($table);
		if (isset($columns[$field]['enum']) && $columns[$field]['enum'] !== "") {
			$enum = new $columns[$field]['enum'];
			$enum = $enum->dataset();
			$enum_string="";
			foreach ($enum as $key=>$value) {
				$enum_string .= $value['enum_name'] . ' => ' . $value['enum_value'] . '<br/>';
			}
			return $enum_string;
		}
			
		else
			return false;
	}
	

	/**
	 * field_structure
	 *
	 * returns the name of the "structure" class (aka, a class to provide structure to a JSON structure)
	 *
	 * @param		string		$table The DBX extension table 
	 * @param		string		$field The column/field name
	 * @return		string 		the class name of the enumeration class that constrains this field; false if not found
	 * @author		Ken Snyder
	*/
	public function field_structure($table,$field) {
		$columns = $this->get_col_ref($table);
		if (isset($columns[$field]['structure']) && $columns[$field]['structure'] !== "")
			return $columns[$field]['structure'];
		else
			return "\stdClass";
	}
	
	

	/**
	* field_uom_context
	* 
	* Passes back the unit-of-measure "context" that a particular DBX column is bounded by. The context is provided by the UOM Contexts taxonomy.
	*
	* @param string  	$table		the DBX table (shortcut name not fully qualified)
	* @param type 		$field		the DBX column
	* @return string				the 'context' that this field/column has
	*/
	public function field_uom_context($table, $field) {
		$column = $this->get_col_ref($table);
		if (isset($column[$field]['uom_context']))
			return $column[$field]['uom_context'];
		else 
			return "";
	}
	
	/**
	 * field_static_uom
	 *
	 * returns the static_uom property. This property is set in cases where the context is static and the insight is marker
	 *
	 * @param		string		$table 
	 * @param		string		$field 
	 * @return		void
	 * @author		Ken Snyder
	*/
	public function field_static_uom($table, $field) {
		$column = $this->get_col_ref($table);
		if (isset($column[$field]['static_uom']))
			return $column[$field]['static_uom'];
		else
			return "";
	}
	
	/**
	 * field_units
	 *
	 * Given a particular table and field, gives back the units of measure that could be used with it.
	 *
	 * @param		string		$table 
	 * @param		string		$field 
	 * @return		void
	 * @author		Ken Snyder
	*/
	public function field_units($table,$field) {
		$insight = $this->field_insight($table,$field);
		$context = $this->field_uom_context($table,$field);
		if ($insight === "marker") {
			if ($context === "static") return $this->field_static_uom($table,$field);
			else {
				$units = new LG_Units();
				AppLogger::debug ( "UNITS: {$field} with context {$context}\n" ); 
				return $units->units_for_context($context);
			}
		} else {
			return "";
		}
	}
	
	/**
	 * enumerations
	 *
	 * returns a list of the valid enumerated elements that constrain the given column
	 *
	 * @param		string		$table 
	 * @param		string		$field 
	 * @return		void
	 * @author		Ken Snyder
	*/
	public function enumerations($table,$field) {
		$column = $this->get_col_ref($table);
		$column = $column[$field];
		if (!isset($column['enum'])) return array();
		else {
			$enum = new $column['enum']();
			$values = $enum->list_values();
			$names = $enum->list_keys();
			$result = array();
			foreach ($values as $value) {
				$pairing['value'] = $value;
				$pairing['name'] = array_shift($names);
				array_push($result,$pairing);
			}
			return $result;
		}
	}
	
	/**
	* field_constraint 
	* 
	* Returns the enumeration or static marker that the field is controlled by. If there is no constraint on the field than the "type" property is set to "none".
	*
	* @param string  		$table 		The DBX table (shortcut name not fully qualified) 
	* @param string  		$field		The DBX column
	* @return DBX_FieldConstraint		The constraints on the given field
	*/
	public function field_constraint($table, $field) {
		// TODO: Finish the implementation of this method
		$column = $this->get_col_ref($table);
		$insight = $this->field_insight($table,$field);
		$response = new DBX_FieldConstraint();
		
		switch ($insight) {
			case "marker":
			
			// a field that stores the unit-of-measure
			case "uom":
			
			// a field which acts as foreign-key to another table
			case "lookup":
			
			// a field bound by an enumeration
			case "static-choice":
			case "static-choice-override":
			
			
			// a field 
			case "static-range":
			
			case "prose":
			
		}
	}
	
	public function dictionary_elements($ext_table) {
		global $wpdb_x;
		return $wpdb_x->get_attributes_meta($ext_table);
	}
	
}
