<?php
/**
 * DBX_FieldGrouping class
 *
 * Groups together related fields in an extension table 
 *
 * @package database-extension-tables
 * @author Ken Snyder
 **/
class DBX_FieldGrouping 
{
	// Const
	
	// Instance Variables
	public $group_name;
	public $slug_name;
	public $ext_table;
	public $group_attributes;


	// Constructor
	function __construct ( $ext_table , $slug_name , $name = false ) {
		global $wpdb_x;
		
		if ( !$name ) $name = DBX_UtilityFunctions::beautify ( $slug_name );
		if ( !$wpdb_x->ext_table_exists ( $ext_table ) ) {
			if (class_exists ('AppLogger') ) AppLogger::warning ( "Tried to create a field grouping to an extension table that doesn't exist: $ext_table"  ); 
			return false;
		}
		$this->group_name = $name;
		$this->slug_name = $slug_name;
		$this->ext_table = $ext_table;
		$this->group_attributes = array();
		$this->group_attributes['fields'] = array();
		$this->group_attributes['filters'] = array();
	}
	
	/**
	* add_elements
	* 
	* Adds the column names that are being mapped to this grouping
	*
	* @param  string/array 	$elements	the list of elements that belong to this group. if elements already exist they will be added.
	* @return void	
	* @author Ken Snyder
	*/
	public function add_fields ( $fields ) {
		if ( !is_array ($fields) ) $elements = array ( $fields );
		$this->group_attributes['fields'] = array_merge ( $this->group_attributes['fields'] , $fields );
		return;
	}
	
	/**
	* get_slug
	* 
	* getter for group_name
	*
	* @param void  
	* @return string The group_name
	* @author Ken Snyder
	*/
	public function get_slug () {
		return $this->slug_name;
	}
	
	/**
	* get_name
	* 
	* getter for group_name
	*
	* @param void  
	* @return string The group_name
	* @author Ken Snyder
	*/
	public function get_name () {
		return $this->group_name;
	}
	
	
	
	/**
	* add_filter 
	* 
	*  Method Description
	*
	* @param  string	$filter_name	The referencible name of the filter
	* @return void
	* @author Ken Snyder
	*/
	public function add_filter  ( $filter_name , $filter_elements ) {
		if ( !is_array( $filter_elements ) ) $filter_elements = array ( $filter_elements );
		if ( isset ( $this->group_attributes['filters'][$filter_name] ))
			if (class_exists ('AppLogger') ) AppLogger::warning ( "Trying to add '$filter_name' filter but it already exists; replacing old version with " . print_r ( $filter_elements , TRUE ) ); 
		$this->group_attributes['filters'][$filter_name] = $filter_elements;
		return;
	}
	
	/**
	* get_fields 
	* 
	* return the list of fields contained in the group
	*
	* @param 	string  	$group_name 	the group name
	* @return 	array 						Return Description
	* @author Ken Snyder
	*/
	public function get_fields () {
		return $this->group_attributes['fields'];
	}

	/**
	* get_filters 
	* 
	* return the list of filters contained in the group
	*
	* @param 	string  	$group_name 	the group name
	* @return 	array 						Return Description
	* @author Ken Snyder
	*/
	public function get_filters () {
		return $this->group_attributes['filters'];
	}
	
	
	/**
	* get_config
	* 
	* Sends the calling function a full hash of the configuration of this object
	*
	* @param 	void
	* @return 	associative_array  	returns an array of the form: [group_name][elements | filters]
	* @author Ken Snyder
	*/
	public function get_config () {
		return array ( $this->group_name => $this->group_attributes );
	}
	

} // END class 




/* --------- END OF FILE ---------- */