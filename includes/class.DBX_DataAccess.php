<?php
	
/**
 * DBX_DataAccess class
 *
 * a library of functions that help in interacting with the database
 *
 * @package database-extension-tables
 * @author Ken Snyder
 **/

define ('DBX_ENGINE' , 'MyISAM');
define ('DBX_CHARSET' , 'utf8');

class DBX_DataAccess 
{
	// Const
	
	// Static Variables
	private $DDL_expansion = array (
				'varchar' => 'varchar ([amt]) NOT NULL DEFAULT [default]',
				'bigint' => 'bigint(20) unsigned NOT NULL DEFAULT 0',
				'datetime' => 'datetime NOT NULL DEFAULT [0000-00-00 00:00:00]',
				'text' => 'text [null]'
				
	);

	// Static Methods
	
	/**
	* db_table_exists
	* 
	* returns true or false based on whether the extension table exists
	*
	* @param string $ext_table the name of the extension table who's existance is being checked
	* @return boolean True if a table by that name already exists in the database
	* @author Ken Snyder
	*/
	public static function db_table_exists ( $ext_table ) {
		global $wpdb;
		global $wpdb_x;
		$my_table = $wpdb_x->get_ext_table_full_name ( $ext_table );
		$my_response = $wpdb->query ( $wpdb->prepare ( 'SHOW tables LIKE %s' , $my_table ) );
		if ( $my_response == 0 ) return false;
		else if ( $my_response == 1 ) return true;
		else {
			error_log ( 'Warning: when checking for the existance of table ' . $my_table . ' it was discovered there were multiple tables with that name (' . $my_response . ')' );
			return true;
		}
	}
	
	public static function db_column_exists ( $ext_table , $column ) {
		global $wpdb, $wpdb_x;
		// TODO: Implement this
	}
	
	public static function add_column_to_database ( $ext_table , $column ) {
		global $wpdb, $wpdb_x;
		$full_table = $wpdb_x->get_ext_table_full_name ( $ext_table );
		$column_def = $wpdb_x->get_column_ddl ( $ext_table , $column );
		$sql_query = "ALTER TABLE {$full_table} ADD {$column} {$column_def->ddl};";
		$db_results = $wpdb->query ( $sql_query );
		if ( $db_results === FALSE ) throw new ErrorException ( "There was an error with executing the SQL DDL when trying to add a column to the database: " . $wpdb->print_error() , DBX_ERROR_ADD_COL_SQL_EXEC_ERROR );
		return true;
	}
	
	public static function db_table_definition ( $ext_table ) {
		global $wpdb;
		global $wpdb_x;
		if ( !self::db_table_exists( $ext_table) ) return false;
		$full_table_name = $wpdb_x->get_ext_table_full_name ( $ext_table );
		if ( ! $full_table_name ) return false;
		$sql_query =<<<QUERY
			SELECT COLUMN_NAME,IS_NULLABLE,COLUMN_TYPE,DATA_TYPE,NUMERIC_PRECISION, COLUMN_DEFAULT, COLUMN_KEY FROM information_schema.`COLUMNS`
			WHERE TABLE_NAME = '$full_table_name'
QUERY;
		$db_results = $wpdb->get_results ( $sql_query );
		foreach ( $db_results as $result ) {
			$results_array [ $result->COLUMN_NAME ] = $result;
		}
		return $results_array;
	}
	
	/**
	* create_table
	* 
	* assuming that the table doesn't exist already, this method will add it based on the specs given 
	* in the column definition hash
	*
	* @param string $tbl_name the name of the extension table to be added
	* @param boolean $create_fk_constraint specifies whether a column defined as FK should be setup as a "KEY" or a "FOREIGN KEY"; since WordPress doesn't use any full FK constraints the majority of these will be without the full constraint system enforced at the database level
	* @return boolean returns true if successful, false if not
	* @author Ken Snyder
	*/
	public static function create_table ( $ext_table , $create_fk_constraint , array $table_meta ) {
		global $wpdb, $wpdb_x;
		
		$tbl_name = $wpdb_x->get_ext_table_full_name ( $ext_table );
		// convenience handles
		$tbl_columns = $table_meta [ 'columns' ];
		$tbl_constraints = $table_meta [ 'constraints' ];
		// reset DDL string
		$DDL = "CREATE TABLE `$tbl_name` (\n";
		// iterate through columns
		foreach ($tbl_columns as $col => $col_attr) {
			$DDL .= "\t`$col` " . $col_attr['type'];
			$DDL .= self::if_defined_then ( $col_attr , 'unsigned' );
			$DDL .= self::if_defined_then ( $col_attr , 'nullable');
			$DDL .= self::default_or_auto_increment_based_on_type ( $col_attr );
			$DDL .= ",\n";
		}
		$DDL .= "\n\tPRIMARY KEY (`" . $tbl_constraints['PK'] . "`)";
		
		foreach ( $tbl_constraints['FK'] as $fk_ref => $fk_target ) {
			if ( $create_fk_constraint ) {
				
			} else {
				$DDL .= ",\n\tKEY `$fk_ref` (`$fk_ref`)";
			}
		}
		
		$DDL .= "\n) ENGINE=" . DBX_ENGINE . " DEFAULT CHARSET=" . DBX_CHARSET;
		error_log ( "DDL to submit to database: \n" . $DDL );		
		$my_response = $wpdb->query ( $DDL );
		if ( $my_response === FALSE ) throw new ErrorException ( "There was an error with executing the SQL DDL: " . $wpdb->print_error() , DBX_ERROR_CREATE_TABLE_SQL_EXEC_ERROR );
		error_log ( 'DDL RESPONSE' . print_r ( $my_response , TRUE ) );
		return true;
	}

	/**
	* row_exists
	* 
	* checks the existance of a row in the database based on extension table and post_id.
	*
	* @param string $ext_table 		the extension table to look at
	* @param string $post_id		the transaction ID to look for
	* @return boolean 				True/false based on whether a row already exists
	* @author Ken Snyder
	*/
	public function row_exists ($ext_table , $post_id ) {
		global $wpdb;
		
		$table_fullname = WP_DBX_PREFIX . $ext_table;
		$sql_query = $wpdb->prepare ( "SELECT id FROM $table_fullname WHERE post_id=%d", $post_id );
		$query_results = $wpdb->get_results ( $sql_query , ARRAY_A );
		if ( $query_results )
			return true;
		else
			return false;
	}
	
	/**
	* get_row
	* 
	* returns a row from an extension table or alternatively a discrete set of column(s) from the row
	*
	* @param string $post_id		the transaction ID to look for
	* @param string $ext_table 		the extension table to look in
	* @param string $column 		(optionally) the column you want returned
	* @return boolean 				True/false based on whether a row already exists
	* @author Ken Snyder
	*/
	public function get_row ($post_id , $ext_table , $columns = false) {
		global $wpdb;
		if ($columns !== false) {
			if (!is_array($columns)) $columns = array ($columns);
			$selector = implode ( ', ', $columns );
		} else  {
			$selector = "*";
		}
		$ext_table = WP_DBX_PREFIX . $ext_table;
		$sql_query = $wpdb->prepare ("SELECT {$selector} FROM {$ext_table} WHERE post_id=%d", $post_id);
		$sql_results = $wpdb->get_results ( $sql_query , ARRAY_A );
		
		if (class_exists ('AppLogger') ) AppLogger::debug ( "QUERY GET-ROW: \n{$sql_query}\n\nRESULT:" . print_r ( $sql_results , TRUE ) ); 
		return $sql_results[0];
	}
	/**
	 * tells the calling function whether the specified column name exists in the DBX table
	 *
	 * @param		string		$table 
	 * @param		string		$column 
	 * @return		boolean
	*/
	public static function column_exists($table,$column) {
		global $wpdb;
		
		$table_fullname = WP_DBX_PREFIX . $table;
		$sql_query = "SHOW COLUMNS FROM {$table_fullname} LIKE '{$column}'";
		\AppLogger::debug ( "ABOUT TO EXECUTE: " . print_r ( $sql_query , TRUE ) ); 
		$wpdb->get_results ( $sql_query , ARRAY_A );
		if ($wpdb->num_rows === 1)
			return true;
		else
			return false;
	}
	
	/**
	 * rename a database column's name
	 *
	 * @param		string		$table 
	 * @param		string		$old_name 
	 * @param		string		$new_name 
	 * @return		void
	*/
	public function rename_column($table,$old_name,$new_name) {
		global $wpdb;
		
		if (DBX_DataAccess::column_exists($table,$old_name)) {
			$table_fullname = WP_DBX_PREFIX . $table;
			$meta = new DBX_Meta();
			$db_type = $meta->physical_type($table,$old_name);

			$sql_query = "ALTER TABLE {$table_fullname} CHANGE {$old_name} {$new_name} {$db_type}";
			\AppLogger::debug ( "ABOUT TO EXECUTE: " . print_r ( $sql_query , TRUE ) ); 
			return $wpdb->get_results ( $sql_query , ARRAY_A );
		} else {
			\AppLogger::debug ( "COLUMN {$old_name} doesn't exist!"); 
			return false;
		}
	}
	
	public function column_count($table) {
		global $wpdb;
		$full_tablename = WP_DBX_PREFIX . $table;
		$sql = "SHOW COLUMNS FROM {$full_tablename}";
		$result = $wpdb->get_results($sql);
		return $wpdb->num_rows;
	}
	
	public function add_column($table,$column,$db_type) {
		global $wpdb;
		$full_tablename = WP_DBX_PREFIX . $table;
		$sql = "ALTER TABLE {$full_tablename} ADD $column $db_type";
		\AppLogger::debug ( "ADD COLUMN: \n {$sql}" ); 
		return $wpdb->get_results($sql);
	}

	/**
	* if_defined_then
	* 
	* allows a shortcut method to display the value of a hash tag if it exists and return an empty string if it doesn't
	*
	* @param hash $hash_values the hash values to look at
	* @return string The value of the attribute if defined otherwise an empty string
	* @author Ken Snyder
	*/
	private static function if_defined_then ($hash_values , $look_for ) {
		if ( isset ( $hash_values[$look_for] ) ) {
			return " " . $hash_values[$look_for];
		} else {
			return "";
		}
	}
	
	/**
	* default_or_auto_increment_based_on_type
	* 
	* inserts the appropriate DLL language for a DEFAULT value or in the case of a Primary Key it returns
	* a AUTO_INCREMENT instead.
	*
	* @param hash $hash_values the attributes associated with the column being operated on
	* @return string The DDL appropriate for this column
	* @author Ken Snyder
	*/
	private static function default_or_auto_increment_based_on_type ($hash_values) {
		$family_type = self::get_family_type( $hash_values['type']);
		
		if ($family_type == "integer" && isset ($hash_values['PK']) ) return " AUTO_INCREMENT";
		else if ( isset($hash_values['default']) ) {
			return " DEFAULT '" . $hash_values['default'] . "'";
		} else {
			return self::get_default_value_for_type_family( $family_type );
		}
	}
	
	/**
	* get_default_value_for_type_family
	* 
	* the appropriate default value for a mySql type if it isn't explictly stated
	*
	* @param string $type_family the family of types that this column belongs to
	* @return string The default value
	* @author Ken Snyder
	*/
	public function get_default_value_for_type_family ($type_family) {
		error_log ("Getting default for family of $type_family");
		$defaults_by_type = array (
				'integer' => NULL,
				'character' => NULL,
				'text' => false,
				'datetime' => NULL,
				'date' => NULL,
				'time' => NULL,
				'year' => NULL
		);
		
		// $defaults_by_type = array (
		// 		'integer' => "'0'",
		// 		'character' => "''",
		// 		'text' => false,
		// 		'datetime' => "'0000-00-00 00:00:00'",
		// 		'date' => "'0000-00-00'",
		// 		'time' => "'00:00:00'",
		// 		'year' => "0000"
		// );
		
		if (!isset ($defaults_by_type[$type_family] ) ) return "";
		else if ($defaults_by_type[$type_family]) return " DEFAULT " . $defaults_by_type[$type_family];
	}
	
	
	/**
	* get_type_family
	* 
	* aggregate mySQL types into their a grouped type of "int", "character", "text", "datetime", etc.
	*
	* @param string $col_type The type definition of the column
	* @return string The family name of the type
	* @author Ken Snyder
	*/
	public static function get_family_type ( $col_type ) {
		$families = array (
					'integer' => array ( 'integer', 'int', 'tinyint', 'mediumint', 'bigint' ),
					'fixed' => array ( 'decimal', 'numeric' ),
					'floating' => array ( 'float' , 'double' ),
					'character' => array ( 'char', 'varchar' ),
					'datetime' => array ( 'datetime' , 'timestamp'),
					'time' => array ('time'),
					'date' => array ('year'),
					'text' => array ('tinytext', 'text', 'mediumtext' , 'longtext'),
					'blob' => array ('blob','tinyblob','mediumblob','longblob')
		);
		
		// first get rid of any optional length parameterisation
		list ( $col_type ) = explode ( "(" , $col_type , 2 );
		foreach ($families as $family => $family_members) {
			if ( in_array ( $col_type , $family_members , true ) ) {
				return $family;
			}
		}
		return "unknown $col_type";
	}
	
	/**
	* default_size_by_type
	* 
	* return the default size for datatypes in mysql; specifically the "integer types" as the number specified is 
	* not actually a "precision" but a suggested presentation attribute. 
	*
	* @param string $col_type The type definition of the column
	* @return string The family name of the type
	* @author Ken Snyder
	*/
	
	public static function default_size_by_type ( $db_type ) {
		switch ( $db_type ) {
			case "tinyint":
				return "3"; // note: depends on whether signed or not
				break;
			case "smallint":
				return "5"; // note: depends on whether signed or not
				break;
			case "int":
				return "10"; // note: depends on whether signed or not; signed is 11, unsigned is 10
				break;
			case "bigint":
				return "20";
				break;
			case "boolean":
			case "bool":
				return "1";
				break;
			default:
				return false;
		}
	}
	
	public static function database_type_alias ( $db_type ) {
		switch ( $db_type ) {
			case "boolean":
			case "bool":
				return "tinyint";
			default:
				return false;
		}
	}
	

} // END class 


/* END OF FILE */