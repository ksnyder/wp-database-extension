<?php
/* 
Build Extensions using the TableExtension class
-----------------------------------------------
note: add_extension_cols() doesn't do any database DDL but simply provides a structure to the code; 
calling the create_table() static method will define the table but if a table by that name already 
exists it will exit (updates of tables will be left as a manual operation).  
*/
$my_ext = new TableExtension ();
$my_ext->add_extension ( 'activity_tbl' , array ( 'auto-hook-use' => 'true' , 'dynamic-attributes' => false ));
$my_ext->add_extension_cols ( 'activity_tbl' , array (
		'start-time' => array ( 'type' => "datetime" , 'desc' => "start-time of the activity" ),
		'end-time' => array ('type' => 'datetime' , 'desc' => "end-time of the activity"),
		'app-id' => array ( 'type' => "largeint" , 'desc' => "the application id that recorded this activity"),
		'app-uri' => array ( 'type' => "decimal(6,2)" , 'desc' => "the URI for details of this activity")
));
$my_ext->add_extension ( 'exercise_tbl' ); // if second param left off defaults are used
$my_ext->add_extension_cols ( 'exercise_tbl' , array (
		'cal-burned' => array ( 'type' => "int" , 'desc' => "the number of estimated calories burned during the exercise" ),
		'distance' => array ('type' => 'decimal(6,2)' , 'desc' => "the distance travelled during this exercise"),
		'distance-uom' => array ( 'type' => "varchar(10)" , 'req' => 'false' , 'desc' => "the unit of measure that this was recorded as"),
		'distance-std-unit' => array ( 'type' => "decimal(6,2)" , 'desc' => "the distance travelled converted to meters")
));
/* 	
Attributes (aka, values stored in the json hash) can be added dynamically if allowed with the 
'dynamic-attributes' option in add_extension() but there will be times where it's benefitial to 
explicitly define the attributes that are "allowed". Also note that you can add a list of attributes
without going to the effort of defining the attributes (type, desc, etc.) if you don't want to be 
explicit. All implicit additions will be added with the assumption of being a string.
*/
$my_ext->add_extension_attrs ( 'exercise_tbl' , array ( 
		'attribute1' => array ( 'type' => "string" , 'desc' => "describe attribute" ),
		'attribute2' => array ( 'type' => "datetime")
));
/*
Calls to this function could be included in a theme's functions.php but this would require an 
unnecessary hit to the database each time a page is loaded to check whether the table exists already.
Instead, it is imagined that there might be a simple admin interface which would list the tables 
defined in code and allow you to click a "add to database" button if the table doesn't exist already
(this same screen could indicate whether the code definition and database definitions are out of sync 
... getting them back in sync would require manual DDL by the user)
*/
$my_ext->create_table ( 'activity' );
/*	
Now that extensions tables have been defined in code the next step is associating Post Types to these
extension tables which is where the PostTypeExtension class comes into play. A post type has a 1:M 
relationship to extension tables. In many cases it's likely that the relationship will be 1:1 but by 
chaining together multiple extension tables you can design much more efficient data storage in a 
TYPE/SUB-TYPE pattern where certain attributes are repeated across post-types (the "TYPE") but each 
individual post-type has it's specific attributes (the "SUBTYPE")
*/
$exercise = new PostTypeExtension ("exercise", &$my_ext); // create an object to manage the "exercise" custom post-type
$exercise->add_extension_table("activity_tbl");
$exercise->add_extension_table("exercise_tbl");
/*
Implicit versus Explicit use of extension tables:
-------------------------------------------------
storing data to custom post types is often done best by explicitly calling this plugins methods but 
for a variety of reasons it may be desirable to have the default WP methods -- get_post_meta, 
add_post_meta, update_post_meta, delete_post_meta -- overriden such that they stop using the wp_postmeta
table and instead use the extension tables.
	
By default the plugin assumes that only explicit calls to the extension tables will work but you can
switch to include the implicit calls by using the method below:
*/
$exercise->force_implicit_use();

/* 
Now that all the "configuration" is out of the way, the remaining examples show the 
how the plugin might used in an application. The basic use cases will be brought up 
first and then we'll move to some "aggregate" functions that might be useful as a 
"phase 2" scope.
*/
// GETers
$list = $exercise->list_attributes(); // lists all the column and attribute headings across all extension tables
$time = $exercise->get_value( $post_id , "start_time" );
$hash = $exercise->get_values( $post_id );
// SETers
$exercise->set_value( $post_id, "start_time" , $start_time ); // updates a single value to the extension table
$exercise->set_values( $post_id , array ( "end_time" => $end_time , "primary_location" => $pri_loc ) ); // same but for multiple values

// Aggregate functions could be added too (maybe a phase 2 feature)
$avg_value = $exercise->avg_value ( $col_name ); // provides the average value for a given numeric value
$avg_value = $exercise->avg_value_user ( $col_name ); // provides the average value for a given numeric value owned by the current user
$max_value = $exercise->max_value ( $col_name ); // provides the max value for a given numeric value
$max_value = $exercise->max_value_user ( $col_name ); // provides the max value for a given numeric value owned by the current user
$min_value = $exercise->min_value ( $col_name ); // provides the min value for a given numeric value
$min_value = $exercise->min_value_user ( $col_name ); // provides the min value for a given numeric value owned by the current user
$std_dev   = $exercise->std_dev ( $col_name ); // provides the std deviation for a given numeric value 
$std_dev   = $exercise->std_dev_user ( $col_name ); // provides the std deviation for a given numeric value owned by the current user
	
	
?>